# -*- coding: utf-8 -*-
"""
Created on Thu Jan 14 08:29:20 2021

@author: mathi
"""
i = 1
y = input('What Fibonacci number would you like? Please enter you index:')

while i <= 1:
    try:
        y = int(y)
    except ValueError:
        print("That's not a number!") 
         
    if isinstance(y,int) and y >= 0:
        if y == 0:
            fib = 0
        elif y== 1:
            fib = 1
        else:
            fib = 1
            x = 2
            fib1 = 0
            fib2 = 0
            while x <= y:
                    fib2 = fib
                    fib = fib1 + fib2
                    fib1 = fib2
                    x = x+1
    
        print ('The Fibonacci number at the {:} index is {:}'. format(y, fib))
        i = i+1
    else:
        y = input('Please enter a valid integer:')
    
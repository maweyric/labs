'''@file        BotUpFib.py
@brief          Returns the Fibonacci Number at a given index
@details        Ensures that a proper index is submitted to identify a Fibonacci
                Number, while repeating the code for as long as the user requires 
                a Fibonacci Number. Use the following link for the source code
                https://bitbucket.org/maweyric/labs/src/master/Lab%201/BotUpFib.py
@author         Mathis Weyrich
@date           Thu Jan 14 09:43:24 2021
@copyright      License Info
'''

def fib (usr):          # This section of the code creates a separate function
    '''@brief   Returns the Fibonacci Number at a given index
    @param      usr The index submitted by the user
    @return     The resulting Fibonacci Number
    '''
    i = 2               # that solves the Fibonacci sequence. This function
    fib1 = 0            # does not actively print any answer, but rather 
    fib2 = 1            # returns the final answer. The function requires
    if usr == 0:        # an initial input (in integer/float form).
        fib = 0
    elif usr == 1:
        fib = 1
    else: 
        while i <= usr:
            fib = fib1 + fib2
            fib1 = fib2
            fib2 = fib
            i = i+1
    return fib          # When calling this function, this value will be used

if __name__=='__main__': # To be able to call the Fibonacci function w/out calling the rest of the code

    yn = 'yes'              # To be able to start my while loop
    while yn == 'yes':      # Loop is designed to keep working if people type 'yes'
        x = 1
        print('\nWhat Fibonacci number would you like?') # \n creates a new line. Looks better
        usr = input('Enter index here: ') 
        while x <=1:
            if usr.isdigit():   # Identifies if the input string is a positive, whole number
                    usr = int(usr)  # Need user to be an integer to use in the Fib Function
                    finalfib = fib(usr)
                    print('\nThe selected Fibonacci Number at {:} is \n--> {:}'. format(usr, finalfib))  
                    # Prints out the answer with the formatted numbers where the {:} are 
                    x = x+1
            else:
                usr = input('Enter a whole number above 0. Please try again: ')
        print('\nWould you like to find another Fibonacci Number?')
        yn = input("Type 'yes' if you would, or 'no' if you dont: ") # To keep loop going if wanted
    else:
        print('\nGreat working with you!')
                
            
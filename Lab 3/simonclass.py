"""
@brief      A class with functions to run and play a Simon Says game
@details    Class includes the following functions: __init__, run, pattern, 
            lights, compare. __init__ sets initial variables. Run activates
            a single round. Pattern is the method that creates the array that will 
            deictate the pattern. The pattern changes every round. Lights is the
            method that turns on the lights based on the array created in the pattern.
            Compare is the method that takes the user input and comparing with
            the corresponding array. 
            Use the following link for the source code:
            https://bitbucket.org/maweyric/labs/src/master/Lab%203/simonclass.py
@author     Mathis Weyrich
@date       Thu Feb 4 08:30:53 2021
@copyright  License Info

"""
import random, utime, pyb, math as np # numpy doesn't work on the board


class simonclass:
    
    
    
    def __init__(self): # Initialzing a lot of global variables
        '''@brief  The initializing function to play Simon Says
        @details   This sets up an LED and creates a variable for it. 
        
        '''
        
        
        self.LED = pyb.Pin (pyb.Pin.cpu.A5) # Setting a certain pin as a variable (LD2)

        self.pat = 0
        self.loss = 0
        self.win = 0
        
        self.tim2 = pyb.Timer(2, freq = 20000) # Not quite sure, was used in the setup of a dimming LED
        self.LEDbright = self.tim2.channel(1, pyb.Timer.PWM, pin=self.LED)
        
        
        
        
    def run(self, difficulty=1): #Default level will be 1
        '''
        @brief     The main program that runs the rest of the Simon Says game
        @details   The method sets the difficulty for the round, preps the user
                   and runs the lights in a specific pattern.
        @return    The end of the method returns true.
        '''
        # This will be for one loop - will increase difficulty each time
        #LED countdown (3 beeps)
        self.difficulty = difficulty
        self.pattern(self.difficulty) # Runs pattern with the associated difficulty level
        
       
        print('The pattern will commence in:')
        print('3')
        pyb.delay(500)
        print('2')
        pyb.delay(500)
        print('1')
        pyb.delay(500)
        print('Now!')
        
        self.lights()
        
        print('Now, you must press the button', self.difficulty, 'times in the same fashion as the lights:')
        
        return True # To be able to be able to tell when the run method is finished
        
        
    def pattern(self, difficulty): # The paterren creating method
        '''
        @brief Creates a brand new pattern based on the round difficulty
        @details A 0 in the pattern indicates a break between each portion of the pattern,
                 a 1 denotes a dim light, a 3 denotes a short light, and a 2 denotes a long light
        '''
        list = [0]
        
        for x in range(0,self.difficulty): #the array has a set of random numbers corresponding to a difficulty level
            list.append(random.randint(1,3)) # 1 = "no" press, 2 = long press, 3 = med press
            list.append(0) # 0 = In between each number (just a time gap)
        
        self.pat = list
        
    def lights(self):
        '''
        @brief Activates the lights based on the pattern
        @details A function in this method turns 4 different numbers in the pattern
                 where the numbers 0,1,3 are the same length and the 2 is twice as long.
                 The light is off for 0. 10% brightness for 1. 100% brightness for 2 and 3. 
        '''
        
        for x in range(0,len(self.pat)): # Converts the pattern into lights
            
            start = utime.ticks_ms() # Creates a reference point for the lights
              
            
            while utime.ticks_diff(utime.ticks_ms(), start-250*(self.pat[x]**3 - 4*self.pat[x]**2 + 3*self.pat[x])) <=500: 
# A filter that gets the pattern numbers 0,1,3 to be the same length, and 2 to be twice as long. Base time is half a second.
                if self.pat[x] > 1:
                    self.LEDbright.pulse_width_percent(100) # pattern number 2-3 are bright
                elif self.pat[x] == 1:
                    self.LEDbright.pulse_width_percent(10) # pattern number 1 is dim
                else:
                    self.LEDbright.pulse_width_percent(0) # pattern number 0 is a spacer with no light
            self.LEDbright.pulse_width_percent(0)
        
        
        
        
    def compare(self,pressBatch):# does a batch process - capture all the data as an array and compare. 
        '''
        @brief This compares the user input values with the pattern values
        @details A function parses the user input from the button to within 
                 a quarter of a second of the actual pattern. The target length
                 for 0 and 1 is zero seconds. The target length for 3 is half a
                 second. The target length for 2 is one second.
        '''
        self.pressBatch = pressBatch
        t = 1
        for x in range(0, len(self.pat)):
#For debugging - print(self.pressBatch[x],'input', 500*(self.pat[x]%(-5/6 * (self.pat[x])**3 + 7/2 * (self.pat[x])**2 - 8/3 * (self.pat[x]) + 1)), 'Pattern', np.fabs(self.pressBatch[x] - 500*(self.pat[x]%(-5/6 * (self.pat[x])**3 + 7/2 * (self.pat[x])**2 - 8/3 * (self.pat[x]) + 1))), 'Difference')
            if np.fabs(self.pressBatch[x] - (500*(self.pat[x]%(-5/6 * (self.pat[x])**3 + 7/2 * (self.pat[x])**2 - 8/3 * (self.pat[x]) + 1)))) > 250:
# Filter that compares the user input to the pattern. Filter results: A pattern value of 0 = 0, 1 = 0, 2 = 1000, 3 = 500
                return False # Will stop recording at this point
            elif t == len(self.pat):
                return True # Sees if the loop has reached the end of the array
            else:
                t = t + 1 
            
        
                
             
            

            
            
            
'''@file SimonSays.py
@brief      Main file to play Simon Says using our current Nucleo board.
@details    The instructions for the game are printed initially, followed by 
            running the simonclass.py class. The file takes care of capturing
            and passing the user input into the simonclass.py. The file also
            handles the round management and an exiting strategy.
            https://bitbucket.org/maweyric/labs/src/master/Lab%203/SimonSays.py
@author     Mathis Weyrich
@date       Thu Feb  4 08:04:27 2021
@copyright  License Info
@imageSize{Lab3.png,height:500px;width:500px}

@image html Lab3.png This is the finite state machine drawing for lab 3 

@htmlonly 
<body>

<iframe width="560" height="315" src="https://www.youtube.com/embed/LnXite9-2y8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</body>
@endhtmlonly

'''
import simonclass, pyb, utime # Have to import the simonclass to be able to use it!

print('Are you ready to play some Simon Says?? \nHeres how this is going to work: \nThe light will either have a long blink, short blink, or no blink (denoted by a dim light). \nYou must replicate the length of the blinks in order by pressing the button \n(Hint #1: a no blink requires a quick press of the button) \n(Hint #2: you do not need to begin pressing the button immediately \nafter your turn begins)' )
print('Three large blinks will preceed the pattern. \nThe pattern will play, and then it will be your turn!')
dif = 1
usr = 'yes'
theGame = simonclass.simonclass() 
Button = pyb.Pin (pyb.Pin.cpu.C13) # Setting a certain pin as a variable (button - B1)
buttonpress = False

but_init = 0
win = 0
loss = 0 # Setting all these variables for future use

def pressed(IRQ_src): # Creating a callback function!
    '''
    @brief callback function once the button is pressed.
    '''
    global buttonpress
    buttonpress = True
    

but_on = pyb.ExtInt(Button, pyb.ExtInt.IRQ_RISING_FALLING, pyb.Pin.PULL_NONE, pressed) 
# The button is activated on both a rising and falling edge, which means that it will activate twice in a single push
but_on.disable() # Disabling the button so no funny business happens before button pressing is needed

pyb.delay(12000) # Adding a delay to allow people to read the instructions. I want this to be a full system delay.

while usr == 'yes':
    pressBatch = [0]
    if dif < 6: # This is to allow people to win when the pass level 6
        if theGame.run(dif) == True:
            but_on.enable()
            while len(pressBatch) < len(theGame.pat): # Matches the amount of button presses to the length of the array
                if buttonpress == True: 
                    if but_init ==0: # Have to have some way of identifying what state the button is in - down or up.
                        but_start = utime.ticks_ms()
                        but_init = 1 # Button down state
                        buttonpress = False
                    elif but_init == 1:
                        but_end = utime.ticks_ms() # When the button is lifted, marks a second point on the timer
                        pressBatch.append(utime.ticks_diff(but_end, but_start)) # Appends the net time of the button push
                        pressBatch.append(0) # Appends an extra 0 cuz the pattern does too. To space out the actual pattern. 
                        but_init = 0
                        buttonpress = False
            but_on.disable()
            
            if theGame.compare(pressBatch) == True: # If the input was good enough, moves on to the next round!
                dif = dif + 1 # Increases the difficulty each time
                usr = input('Congrats! you made it. \nAre you ready to move on to the next level? Type "yes" or "no"\n')
            else:
                loss = loss + 1 # Making a counter to make a win/loss counter when you are done playing
                print('better luck next time!')
                print('You got to difficulty number', dif)
                usr = input('Do you want to play again? Type "yes" or "no"\n')
                dif = 1 # Resets the difficulty for the next round
    else:
        print('Wow, great job! Looks like you beat the game!') 
        win = win + 1 # Same as above but for wins!
        usr = input('Do you want to play again? Type "yes" or "no"\n')
        dif = 1
if usr == 'no':
    print('Looks like you won',win,'rounds and lost',loss,'rounds.')
    # Exit strategy.
    
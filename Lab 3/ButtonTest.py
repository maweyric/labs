# -*- coding: utf-8 -*-
"""
Created on Fri Feb  5 14:07:06 2021

@author: mathi
"""

import pyb, utime

LED = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000) # Not quite sure, was used in the setup of a dimming LED
LEDbright = tim2.channel(1, pyb.Timer.PWM, pin=LED)


Button = pyb.Pin (pyb.Pin.cpu.C13) # Setting a certain pin as a variable (button - B1)
buttonpress = False
pressBatch = [0]
but_init = 0
z = 0
y = 0

def pressed(IRQ_src):
    global buttonpress
    buttonpress = True
    
    

but_on = pyb.ExtInt(Button, pyb.ExtInt.IRQ_RISING_FALLING, pyb.Pin.PULL_NONE, pressed) 

pat = [0, 1, 0, 2, 0, 3, 0]


LEDbright.pulse_width_percent(100)
pyb.delay(750)
LEDbright.pulse_width_percent(0)
pyb.delay(500)
LEDbright.pulse_width_percent(100)
pyb.delay(500)
LEDbright.pulse_width_percent(0)
pyb.delay(500)
LEDbright.pulse_width_percent(100)
pyb.delay(750)
LEDbright.pulse_width_percent(0)
pyb.delay(750)

for x in range(0,len(pat)): # Converts the pattern into lights
     

    start = utime.ticks_ms() # Creates a reference point for the lights
            
            
    while utime.ticks_diff(utime.ticks_ms(), start) <=1000: #-250*(pat[x]**3 - 4*pat[x]**2 + 3*pat[x])
# A filter that gets the pattern numbers 0,1,3 to be the same length, and 2 to be twice as long. Base time is half a second.
        if pat[x] > 1:
            LEDbright.pulse_width_percent(100) # pattern number 2-3 are bright
        elif pat[x] == 1:
            LEDbright.pulse_width_percent(10) # pattern number 1 is dim
        else:
            LEDbright.pulse_width_percent(0) # pattern number 0 is a spacer with no light
    LEDbright.pulse_width_percent(0)
    
while z <10:
    LEDbright.pulse_width_percent(100)
    pyb.delay(250)
    LEDbright.pulse_width_percent(0)
    pyb.delay(250) 
    z = z+1
    
LEDbright.pulse_width_percent(100)
pyb.delay(500)
LEDbright.pulse_width_percent(0)
pyb.delay(250)
LEDbright.pulse_width_percent(100)
pyb.delay(500)
LEDbright.pulse_width_percent(0)
pyb.delay(250) 

for q in range(0,len(pat)):
    if pat[q] > 1:
        LEDbright.pulse_width_percent(100) # pattern number 2-3 are bright
        pyb.delay(250)
    elif pat[q] == 1:
        LEDbright.pulse_width_percent(10) # pattern number 1 is dim
        pyb.delay(250)
    else:
        LEDbright.pulse_width_percent(0) # pattern number 0 is a spacer with no light
        pyb.delay(250)
    LEDbright.pulse_width_percent(0)
    pyb.delay(250)
    
for w in range(0,len(pat)):
    while y < 8:
        if pat[w] > 1:
            LEDbright.pulse_width_percent(100) # pattern number 2-3 are bright
            pyb.delay(250)
        elif pat[w] == 1:
            LEDbright.pulse_width_percent(10) # pattern number 1 is dim
            pyb.delay(250)
        else:
            LEDbright.pulse_width_percent(0) # pattern number 0 is a spacer with no light
            pyb.delay(250)
        LEDbright.pulse_width_percent(0)
        pyb.delay(250)
        y = y+1
        
start = utime.ticks_ms() # Creates a reference point for the lights
            
            
while utime.ticks_diff(utime.ticks_ms(), start) <=9000: #-250*(pat[x]**3 - 4*pat[x]**2 + 3*pat[x])
# A filter that gets the pattern numbers 0,1,3 to be the same length, and 2 to be twice as long. Base time is half a second.
    LEDbright.pulse_width_percent(100) # pattern number 2-3 are bright
    pyb.delay(500)
    LEDbright.pulse_width_percent(10)
    pyb.delay(500)
    LEDbright.pulse_width_percent(0)
    pyb.delay(250)
    LEDbright.pulse_width_percent(100)
    pyb.delay(500)
    LEDbright.pulse_width_percent(0)
    pyb.delay(250) 
    LEDbright.pulse_width_percent(100) # pattern number 2-3 are bright
    pyb.delay(500)
    LEDbright.pulse_width_percent(10)
    pyb.delay(500)
    
for m in range(0,len(pat)):
    while y < 8:
        if pat[m] > 1:
            LEDbright.pulse_width_percent(100) # pattern number 2-3 are bright
            pyb.delay(250)
        elif pat[m] == 1:
            LEDbright.pulse_width_percent(10) # pattern number 1 is dim
            pyb.delay(250)
        else:
            LEDbright.pulse_width_percent(0) # pattern number 0 is a spacer with no light
            pyb.delay(250)
        LEDbright.pulse_width_percent(0)
        pyb.delay(250)
        y = y+1    

# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 11:51:22 2021

@author: mathi
"""
# Will just be the math bot to output the correction number with inputs
# Inputs:
    # Desired RPM
    # Actual RPM
    # Adjusting Kp'

class ClosedLoop:
    
    def __init__(self, Des_rpm,Act_rpm,kp,Act_pwm):
        self.Des_rpm = Des_rpm
        self.Act_rpm = Act_rpm
        self.Act_pwm = Act_pwm
        self.Kp = kp
        self.pwm_max = 99
        self.pwm_min = -99
        
    def run(self): #Computes and returns the actuation value
        self.delta_pwm = self.Kp*(self.Des_rpm-self.Act_rpm)
        self.Correc_pwm = self.Act_pwm + self.delta_pwm
        if self.Correc_pwm > self.pwm_max:
            return self.pwm_max
        elif self.Correc_pwm < self.pwm_min:
            return self.pwm_min
        else:
            return self.Correc_pwm
    
    
    def get_Kp(self):
        return self.Kp
        
    def set_Kp(self,usr):
        self.Kp = usr
        
        

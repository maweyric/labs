"""
@file       UI_task_w2.py
@brief      Interfaces directly with the computer for the nucleo
@details    Returns data based on the data transmitted to the nucleo. Communication
            between different tasks is done using the shares file.
@author     Mathis Weyrich
@date       Sat Mar  6 15:38:24 2021
"""


from pyb import UART
import shares
from shares import *

class UI_task:
    
    def __init__(self):
        '''
        @brief Initializes the communication port
        '''
        self.myuart = UART(2) # Reference to the physical port that will work
        self.state = 0
        
    def run(self):
        '''
        @brief Transmits data from and to the computer based on various data inputs
        @details Sends data collected in CSV format using an initial passcode for length,
                 Sends the position of the encoder and the delta in position.
        '''
        if self.state == 0:
            if self.myuart.any() != 0:
                shares.User_Input = self.myuart.readchar()
                print('user Input = ' + str(shares.User_Input))
                # if shares.User_Input == 71 or shares.User_Input == 103:#G
                #     self.state = 1# Getting the Data
                # if shares.User_Input == 84 or shares.User_Input == 116:#s
                #     self.state = 2 # Sending a preliminary data packet to tell the length of the data to the computer side
                # if shares.User_Input == 122 or shares.User_Input == 90: #Z
                #     self.state = 3 # Setting The position to 0
                # if shares.User_Input == 112 or shares.User_Input == 80: #P
                #     self.state = 4 # Print encoder position
                #     shares.User_Input = 0
                # if shares.User_Input == 100 or shares.User_Input == 68: #d
                #     self.state = 5 # Print delta
                #     shares.User_Input = 0
                
                
        if shares.Data_send == True:
            if shares.loop > 0:
                self.myuart.write('{:},{:}\r\n'.format(1,shares.loop))
                shares.loop = 0
                print('pass code sent')
                for x in range (len(shares.data)):
                    self.myuart.write('{:},{:}\r\n'.format(shares.tims[x],shares.data[x]))
                shares.tims.clear()
                shares.data.clear()
                shares.Data_send == False
                print('data sent')
                
            
                                    
        elif shares.position[0] !=0: #P
            self.myuart.write(str(shares.position[1]))
            shares.position = (0,shares.position[1])
            print('Position Shared')
            
        elif shares.delta[0] !=0: #d
            self.myuart.write(str(shares.delta[1]))
            shares.delta = (0,shares.delta[1])
            print('delta shared')
            
            
        
"""
@file       pyenc.py
@brief      The driver that interfaces with the encoder.
@details    Creates an encoder driver using pin and timer inputs. 
@author     Mathis Weyrich
@data       Sat Feb 27 15:36:51 2021


"""

# PB6 = TIM4_CH1, PB7 = TIM4_CH2
# PC6 = TIM3_CH1, PC7 = TIM3_CH2
import pyb
# enc_pb6 = pyb.Pin.board.PB6
# enc_pb7 = pyb.Pin.board.PB7
# tim4 = pyb.Timer(4)
# tim4.init(prescaler =0, period = 65535) # The period needs to be the largest 16-bit number it appears minus 1
# pb6 = tim4.channel(1, pyb.Timer.ENC_AB, pin=enc_pb6)
# pb7 = tim4.channel(2, pyb.Timer.ENC_AB, pin=enc_pb7) 

class pyenc:
    #PINA = Pin A (simply as name of pin, ie PB6)
    #PINB = Pin B (simply as name of pin, ie PB7 )
    #TIM = Timer number correlating to the above pins. (ie for PB6/PB7 enter 4)
    #Pres = Prescaler value
    #EncCycle = Number of encoder cycles in one revolution
    def __init__(self,PINA,PINB,TIM,Pres,EncCyc): #Enter the pin Names, and Timer Number
        '''
        @brief Initializes the encoder driver using given pins, timer, prescaler and encoder cycles
        @param PINA     The first pin object in the Encoder
        @param PINB     The second pin object in the Encoder
        @param TIM      The timer number corresponding the PINA and PINB
        @param Pres     The prescaler value to set how often the encoder reads
        @param EncCyc   The number of encoder cycles
        '''
        
        # self.PINA = pyb.Pin.board.PINA
        
        # self.PINB = pyb.Pin.board.PINB
        
        self.PINA = PINA
        self.PINB = PINB
        
        self.TIMer = pyb.Timer(TIM)
        self.Pres = Pres # 0
        self.EncCyc = EncCyc #16384
        self.Per = 4*EncCyc - 1 
        self.TIMer.init(prescaler=self.Pres,period=self.Per)
        self.TIMer.channel(1,pyb.Timer.ENC_AB,pin = self.PINA)
        self.TIMer.channel(2,pyb.Timer.ENC_AB,pin = self.PINB)
        self.prevcount = self.TIMer.counter()
        self.pos = 0
        self.count = 0
        
        
    def update(self):
        '''
        @brief Updates the position of the encoder. Ensures that overflow does not occur.

        '''
        self.prevcount = self.count
        self.count = self.TIMer.counter()
        if (self.count - self.prevcount)> self.Per/2:
            self.count = self.count - self.Per
        elif (self.count - self.prevcount) < -self.Per/2:
            self.count = self.count + self.Per
            
        self.delta = self.count - self.prevcount
        
        self.pos = self.pos + self.delta
        
    def get_position(self):
        '''
        @brief Returns the position of the encoder
        '''
        return self.pos
        
    def set_position(self,usrpos):
        '''
        @brief Sets the position to a given value
        @param usrpos The position that the user wants the encoder to be set at

        '''
        self.pos = usrpos
        
    def get_delta(self):
        '''
        @brief Returns the change in position

        '''
        return self.delta
        
    # def set_reading_speed(self, w,PPR):
    
    #     self.w = w #Motor speed (RPM)
    #     self.PPR = PPR #Resolution (Ticks/Rev)
    #     self.w_ticks = self.w/60 * self.PPR 
    #     self.Pres = (self.Per)/(2*self.w_ticks)
    #     self.TIMer.init(prescaler=self.Pres,period=self.Per)
    #     self.TIMer.channel(1,pyb.Timer.ENC_AB,pin = self.PINA)
    #     self.TIMer.channel(2,pyb.Timer.ENC_AB,pin = self.PINB)
        
    
        


        
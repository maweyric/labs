"""
@file       Control_task_pt2.py
@brief      Is the interface between the encoder and UI on the nucleo
@details    Creates a link between the user input variables and the encoder
            driver. This allows for multiple tasks to be run simultaneously.
            The Control Task interfaces with the UI task using a separate variable
            file called shares.py. 
@author     Mathis Weyrich
@date       Sat Mar  6 15:20:06 2021
@copyright  License Info    
"""

import utime,math as m
import pyb,pyenc,shares
from shares import *

class Control_task:
    
    def __init__(self):
        '''
        @brief Initializes variables and the encoder driver

        '''
        
        self.enc = pyenc.pyenc(pyb.Pin.board.PB6,pyb.Pin.board.PB7,4,0,16384)
        #self.var = shares()
        

        self.state = 0
        self.loop = 1
        
            
    def run(self):
        '''
        @brief      Operates the encoder as a FSM based on user inputs. 
        @details    Allows data to be collected, position and delta position 
                    to be sent, and the encoder position to be zeroed. Ensures
                    encoder is constantly updated.
        '''
        
        self.enc.update()
        if self.state == 0:
            
            
            if shares.User_Input == 71 or shares.User_Input == 103:#g
                self.state = 1# Getting the Data
                self.Tstart = utime.ticks_ms()
                print('Tstart '+str(self.Tstart))
                shares.User_Input = 0
            # if shares.User_Input == 83 or shares.User_Input == 115:
            #     self.state = 2 # Sending a preliminary data packet to tell the length of the data to the computer side
            #     shares.User_Input = 0
            if shares.User_Input == 122 or shares.User_Input == 90: #Z
                self.state = 3 # Setting The position to 0
                shares.User_Input = 0
            if shares.User_Input == 112 or shares.User_Input == 80: #P
                self.state = 4 # Print encoder position
                shares.User_Input = 0
            if shares.User_Input == 100 or shares.User_Input == 68: #d
                self.state = 5 # Print delta
                shares.User_Input = 0
                
               
    
        if self.state == 1:            
            # if self.myuart.any() != 0:
            #     self.val = self.myuart.readchar()
            #     print('reading')
                             
            if utime.ticks_diff(utime.ticks_ms(),self.Tstart) >= 30001:#0:
                self.state = 2
                #self.myuart.write(str(self.loop))
                print('timed out')
                
            elif shares.User_Input == 83 or shares.User_Input==115:
                self.state = 2
                print('Manually changed')
                
            else:
                if utime.ticks_diff(utime.ticks_ms(), self.Tstart) >= 200*self.loop:
                    print(utime.ticks_diff(utime.ticks_ms(), self.Tstart))
                    shares.data.append(self.enc.get_position())
                    shares.tims.append(utime.ticks_diff(utime.ticks_ms(), self.Tstart)/1000)
                    self.loop += 1
                    
    

        if self.state == 2:
            print('state 2')
            shares.loop = self.loop
            shares.Data_send = True
            self.loop = 0
            self.state = 0
            
        if self.state == 3:
            print('state 3')
            self.enc.set_position(0)
            self.state = 0
            
        if self.state == 4:
            print('state 4,' + str(self.enc.get_position()))
            shares.position = (1,self.enc.get_position())
            self.state = 0
            
        if self.state == 5:
            print('state 5,' + str(self.enc.get_delta()))
            shares.delta = (1,self.enc.get_delta())
            self.state = 0
"""
@file       main.py
@brief      A small program allowing the program to be run using two ports for better debugging
@details    Initializes shares first to make sure all the initial data is not 
            altered. Perpetually runs the UI task and Control Task.
@author     Mathis Weyrich
@date       Sun Feb 28 13:16:59 2021

"""
import Control_task, UI_task,pyb,shares
from shares import *

pyb.repl_uart(None)
# myuart = UART(2)
# while True:
#     if myuart.any() != 0:
#         val = myuart.readchar()
        
#         if val == 104:
#             myuart.write(str(val))
#         else:
#             myuart.write(str(val))
UI=UI_task.UI_task()
Control=Control_task.Control_task()
while True:
    UI.run()
    Control.run()
    

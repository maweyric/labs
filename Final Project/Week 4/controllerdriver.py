"""
@file       controllerdriver.py
@brief      Calculates the new PWM to adjust to based on the desired and actual RPM
@author     Matthew Nagy (Adapted code from Matt to work )
"""


class ClosedLoop:
    
    def __init__(self):
        ## @brief Set the maximum PWM of the motor
        
        self.pmax = 99.5 # Max PWM
        ## @brief Set the minimum PWM of the motor
        
        self.pmin = -99.5 # Min PWM 
        self.pwm = 30 # Current PWM
        self.kp = 0 # Gain Constant
        self.deltapwm = 0 # Last Change in PWM
        self.newpwm = 30
       
    
    def update(self,speedcurrent,speedrequest,kp):
        '''
        @brief Updates the PWM to adjust RPM
        @details Uses the current speed, desired speed and adjusting
                constant to find a new PWM. Surrently uses a less accurate 
                but simpler method to calculate the new PWM. This method does 
                not allow the rpm to actual fall exactly at the desired number.
        @param speedcurrent The current RPM of the motor
        @param speedrequest The desired RPM of the motor
        @param kp The scaling constant used in the closed loop equation

        '''
        self.speedreq = speedrequest ## Request this from controller task 
        self.speedcurrent = speedcurrent
        self.kp = kp
        self.deltapwm = self.kp*(self.speedreq - self.speedcurrent)
        self.newpwm = self.newpwm + self.deltapwm
        # print('This is newpwm',self.newpwm)
        if self.newpwm >= self.pmin and self.newpwm <= self.pmax:
            self.pwm = self.pwm + self.deltapwm
            # print('Changing Speed')
            # print('This is delta pwm', self.deltapwm)
            #print('This is PWM', self.pwm, 'This is delta pwm', self.deltapwm)
            
            return self.pwm
        else:
            self.pwm = self.pmax
            # print('Max Or Minimum Power Achieved')
            self.newpwm = self.pmax
            return self.pwm
    
            
       
    
    def get_Kp(self): # Sends out KP to place calling it
        '''
        @brief Returns the current KP
        '''
        return self.kp
    
    def set_Kp(self, newkp): # Sets kp to current kp from controller task
        '''
        @brief Sets the Kp to a new user input
        @param newkp The new Kp desired
        '''
        self.kp = newkp
        
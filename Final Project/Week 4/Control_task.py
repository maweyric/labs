"""
@file       Control_task.py
@brief      Is the interface between the encoder and UI on the nucleo
@details    Creates a link between the user input variables, the encoder
            driver, the motor driver, and the closed loop task. This allows for multiple tasks to be 
            run simultaneously.
            The Control Task interfaces with the UI task using a separate variable
            file called shares.py. 
@author     Mathis Weyrich
@date       Sat Mar  6 15:20:06 2021
@copyright  License Info    
"""

import utime,math as m
import pyenc,shares,MotorDriver
from controllerdriver import ClosedLoop
from shares import *
from pyb import Pin

class Control_task:
    
    def __init__(self):
        '''
        @brief Initializes variables, the encoder driver, the Motor Driver, and the closed loop controller

        '''

        ## @brief Initializing the encoder 
        # @details Set the first 2 variables as different pins, the fourth is the timer
        #           number, fifth is the prescalar, and the sixth is the encoder cycles
        
        self.enc = pyenc.pyenc(Pin.board.PB6,Pin.board.PB7,4,0,16384)
        ## @brief Initializing the motor 
        # @details Set the first variable as the sleep pins, the second and third pins
        #           as the motor pins, and the fourth as the timer
        
        self.moe = MotorDriver.MotorDriver(Pin.cpu.A15, Pin.cpu.B4, Pin.cpu.B5, 3) # Activating the motor that pairs with timer 4 (closest to power)
        
        ## @brief Creating a variable fot the closed loop PWM adjuster
        self.mycontroller = ClosedLoop()
        self.cor = 0
        self.state = 0
        self.loop = 1
        self.Mstate = 0
        self.ms = 0
        
        ## @brief Adjust ticks per revolution to fit encoder
        self.tpr = 4000
        
            
    def run(self):
        '''
        @brief      Operates the encoder as a FSM based on user inputs. 
        @details    Allows data to be collected, position and delta position 
                    to be sent, and the encoder position to be zeroed. Ensures
                    encoder is constantly updated. Runs the Motor controller as 
                    requested.
        '''
        
        self.enc.update()
        self.mycontroller.set_Kp(shares.kp)
        if self.state == 0:
            
            
            if shares.User_Input == 71 or shares.User_Input == 103:
                self.state = 1# Getting the Data
                self.Tstart = utime.ticks_ms()
                self.enc.set_position(0)
                print(self.enc.get_position())
                shares.User_Input = 0
            # if shares.User_Input == 83 or shares.User_Input == 115:
            #     self.state = 2 # Sending a preliminary data packet to tell the length of the data to the computer side
            #     shares.User_Input = 0
            if shares.User_Input == 122 or shares.User_Input == 90: #Z
                self.state = 3 # Setting The position to 0
                shares.User_Input = 0
            if shares.User_Input == 112 or shares.User_Input == 80: #P
                self.state = 4 # Print encoder position
                shares.User_Input = 0
            if shares.User_Input == 100 or shares.User_Input == 68: #d
                self.state = 5 # Print delta
                shares.User_Input = 0
            if shares.User_Input == 119 or shares.User_Input == 87: #W        
                shares.kp_get = True
                shares.User_Input = 0
            if shares.User_Input == 114 or shares.User_Input == 82: # r
                self.Mstate = 1
                shares.rpm_get = True
                shares.User_Input = 0
                self.moe.enable()
                self.enc.set_position(0)
                self.ms = 2
            if shares.User_Input == 99 or shares.User_Input == 67: #c
                shares.User_Input = 0
                shares.csv_read = True
                self.moe.enable()
                self.enc.set_position(0)
                self.Mstate = 2
                self.ms = 4
                print('C in Control')
                
            if shares.User_Input == 110 or shares.User_Input == 78: #n
                self.moe.disable()
                shares.User_Input = 0
                self.Mstate = 0
               
    
        if self.state == 1:  
            # if self.myuart.any() != 0:
            #     self.val = self.myuart.readchar()
            #     print('reading')
                             
            if utime.ticks_diff(utime.ticks_ms(),self.Tstart) >= 30001:#0:
                self.state = 2
                #self.myuart.write(str(self.loop))
                print('timed out')
                
            elif shares.User_Input == 83 or shares.User_Input==115:
                self.state = 2
                print('Manually changed')
                
            elif shares.User_Input == 119 or shares.User_Input == 87: #W        
                shares.kp_get = True
                shares.User_Input = 0
                
            else:
                if utime.ticks_diff(utime.ticks_ms(), self.Tstart) >= 200*self.loop:
                    print('position data ',utime.ticks_diff(utime.ticks_ms(), self.Tstart),self.enc.get_position(),self.enc.get_delta(),shares.act_rpm)
                    shares.pos.append(self.enc.get_position())
                    shares.speed.append(shares.act_rpm)
                    shares.tims.append(utime.ticks_diff(utime.ticks_ms(), self.Tstart)/1000)
                    self.loop += 1
                    
    

        if self.state == 2:
            print('state 2')
            shares.loop = self.loop
            shares.Data_send = True
            self.loop = 0
            self.state = 0
            
        if self.state == 3:
            print('state 3')
            self.enc.set_position(0)
            self.state = 0
            
        if self.state == 4:
            print('state 4,' + str(self.enc.get_position()))
            shares.position = (1,self.enc.get_position())
            self.state = 0
            
        if self.state == 5:
            print('state 5,' + str(self.enc.get_delta()))
            shares.delta = (1,self.enc.get_delta())
            self.state = 0
            
        if self.Mstate == 2:
            if shares.csv_finish == True:
                
                self.MotorStabilizer()
                
            
        if self.Mstate == 1:
            if shares.des_rpm != 0:
                self.MotorStabilizer()
            
           
            
                
    def MotorStabilizer(self):
         '''
        @brief      Operates the motor driver to find the actual rpm based on input pwm. 
        @details    Calculates the actual RPM of the motor using the encoder 
                    position data. This is then sent to the closed loop controller
                    to adjust the PWM based on how close the actual RPM is to the
                    desired value.
         '''
         if self.ms == 1 : # Get Velocity RPM
            self.starttime = utime.ticks_ms()
            self.enc.update()
            self.delta = self.enc.get_delta()
            self.tdiff = utime.ticks_diff(utime.ticks_ms(),self.starttime)
            
            # print('This is the position', self.position)
            if self.tdiff > 0:
                shares.act_rpm = ((self.delta/self.tpr)/(self.tdiff*4))*60000000 # RPM
            elif self.tdiff == 0:
                pass
            self.ms = 2
            # print(shares.record)
            # if shares.record == True: # Only saves these values if record command is true 
            #     shares.velocity.append(self.velocity)
            #     shares.times.append(utime.ticks_ms())
            # print('This is RPM',self.velocity)
            # print('This is change in ticks', self.delta)
        
         elif self.ms == 2: # Updates From Controller
            # print('This is the actual RPM ',shares.act_rpm)
            self.pwm = self.mycontroller.update(shares.act_rpm,shares.des_rpm,shares.kp)
            #shares.kp = self.mycontroller.get_Kp()
            self.ms = 3
            
        
         elif self.ms == 3 : # Updates Motor Duty Cycle
            self.moe.set_duty(self.pwm)
            self.ms =1
         #    if shares.record == False:
         #        self.ms = 1
         #    if self.reset == True:
         #        self.tstart = utime.ticks_ms()
         #        self.reset = False 
         #        self.ms = 4
         #    if shares.record == True:
         #        self.ms = 4
            
         elif self.ms == 4: # Writes positional data to shares iterating over a set time 
             # Going to need time handling logic here
             # print('In state 4 of Con Task')
            self.starttime = utime.ticks_ms()
            self.x = 0
            self.ms = 5
            #print('ms = 4')
            
         elif self.ms == 5:
            
            
            if utime.ticks_diff(utime.ticks_ms(),self.starttime) < shares.csv_time[(len(shares.csv_time)-1)]*1000:
                
                self.enc.update()
                self.delta = self.enc.get_delta()
                self.tdiff = utime.ticks_diff(utime.ticks_ms(),self.starttime)
                print(self.tdiff,shares.csv_time[self.x]*1000,self.x)
                
            
                if self.tdiff > shares.csv_time[self.x]*1000:
                    
                    shares.act_rpm = ((self.delta/self.tpr)/(self.tdiff*4))*60000000 # RPM
                    self.pwm = self.mycontroller.update(shares.act_rpm,shares.csv_speed[self.x],shares.kp)
                    shares.pos.append(self.enc.get_position())
                    shares.speed.append(shares.act_rpm)
                    shares.tims.append(utime.ticks_diff(utime.ticks_ms(), self.starttime)/1000)
                    self.moe.set_duty(self.pwm)
                    self.x +=2
            else:
                shares.csv_moved = True
                print('booo')
                self.Mstate = 0
                self.moe.disable()
                shares.loop = self.x
                

                
                
"""
@file       UI_task.py
@brief      Interfaces directly with the computer for the nucleo
@details    Returns data based on the data transmitted to the nucleo. Communication
            between different tasks is done using the shares file. Sends stored
            data, and recieves sent motor speed data
@author     Mathis Weyrich
@date       Sat Mar  6 15:38:24 2021
"""


from pyb import UART

import shares,pyb
from shares import *

class UI_task:
    
    def __init__(self):
        ## @brief Setting the UART communication channel
        self.myuart = UART(2) # Reference to the physical port that will work
        self.state = 0
        self.var = 0
        self.boi = list()
        
    def run(self):
        if self.state == 0:
            if shares.rpm_get == True:
                self.state =1     
                
            if shares.kp_get == True:
                self.state = 2
                
            if shares.csv_read == True:
                self.state = 3
                
            if shares.csv_moved == True:
                self.state = 4
            
            
            if self.myuart.any() != 0:
                shares.User_Input = self.myuart.readchar()
                print('user Input = ' + str(shares.User_Input))
                # if shares.User_Input == 71 or shares.User_Input == 103:#G
                #     self.state = 1# Getting the Data
                # if shares.User_Input == 84 or shares.User_Input == 116:#s
                #     self.state = 2 # Sending a preliminary data packet to tell the length of the data to the computer side
                # if shares.User_Input == 122 or shares.User_Input == 90: #Z
                #     self.state = 3 # Setting The position to 0
                # if shares.User_Input == 112 or shares.User_Input == 80: #P
                #     self.state = 4 # Print encoder position
                #     shares.User_Input = 0
                # if shares.User_Input == 100 or shares.User_Input == 68: #d
                #     self.state = 5 # Print delta
                #     shares.User_Input = 0
                
                
            if shares.Data_send == True:
                if shares.loop > 0:
                    self.myuart.write('{:},{:}\r\n'.format(1,shares.loop))
                    shares.loop = 0
                    print('pass code sent')
                    for x in range (len(shares.pos)):
                        self.myuart.write('{:},{:},{:}\r\n'.format(shares.tims[x],shares.pos[x],shares.speed[x]))
                    shares.tims.clear()
                    shares.pos.clear()
                    shares.speed.clear()
                    shares.Data_send == False
                    print('data sent')
                    
                
                                        
            elif shares.position[0] !=0: #P
                self.myuart.write(str(shares.position[1]))
                shares.position = (0,shares.position[1])
                print('Position Shared')
                
            elif shares.delta[0] !=0: #d
                self.myuart.write(str(shares.delta[1]))
                shares.delta = (0,shares.delta[1])
                print('delta shared')
                
            elif shares.rpm[0] !=0: #d
                self.myuart.write(str(shares.rpm[1]))
                shares.rpm = (0,shares.rpm[1])
                print('rpm shared')
                
        if self.state == 1:
             if self.myuart.any() != 0:
                    pyb.delay(5)
                    self.var = self.myuart.readline()
                    
                    shares.des_rpm = int(self.var)
                    print('Desired RPM '+str(shares.des_rpm))
                    shares.rpm_get = False
                    self.state = 0
                    # if self.var == 'a':
                    #     for x in range(len(self.boi)):
                    #         shares.des_rpm = self.boi[x]*10**(len(self.boi)-x) + shares.des_rpm
                    #     print('Desired RPM '+str(shares.des_rpm))
                    #     shares.rpm_get = False
                    #     self.state = 0
                    # else:
                    #     self.boi.append(int(self.var))

                        
        if self.state == 2:
             if self.myuart.any() != 0:
                    self.beg_Z = self.myuart.readchar()
                    self.postdec = self.myuart.readline()
                    
                    
                    shares.kp = float(self.postdec)
                    print('Desired RPM '+str(shares.kp))
                    shares.kp_get = False
                    self.state = 0
                    
        if self.state == 3:
            if self.myuart.any() != 0:
                z = self.myuart.readline()
                shares.csv_read = False
                shares.csv_finish = True
                self.state = 0
            # if self.myuart.any() != 0:
            #     pyb.delay(1)
            #     self.password = self.myuart.readline()
            #     self.password = self.password.strip()
            #     print(self.password)
            #     for x in range(int(self.password)):
            #         pyb.delay(1)
            #         self.var1 = self.myuart.readline()
            #         #self.var2 = self.myuart.readline()
            #         self.rough_data1 = self.var1.strip()
            #         #self.rough_data2 = self.var2.strip()
            #         if len(self.rough_data1) != 0:
            #             # self.polished_data = self.rough_data.split(',')
            #             shares.csv_speed.append(float(self.rough_data1))
            #             shares.csv_time.append(0.001+0.050*x)
            #             #shares.csv_time.append(float(self.rough_data2))
            #     shares.csv_read = False
            #     shares.csv_finish = True
            #     print(shares.csv_time)
            #     print(shares.csv_speed)
            #     self.var = 0            
        
                    
        if self.state == 4:
            self.myuart.write('{:},{:}\r\n'.format(1,len(shares.csv_time)))

            print(len(shares.pos))
            for x in range (len(shares.pos)):
                self.myuart.write('{:},{:},{:}\r\n'.format(shares.tims[x],shares.pos[x],shares.speed[x]))
                
            shares.tims.clear()
            shares.pos.clear()
            shares.speed.clear()
            shares.Data_send = False
            shares.csv_moved = False
            print('data sent')
            self.state = 0
                        
            
    
            
        

            
            
        
"""
@file       ComSide.py
@brief      The computer side of a direction communication link 
@details    The computer initializes a serial port with the nucleo for direct
            communication. The computer sends a specific string based on a 
            keyboard input. The potential inputs include:
                g = Start data collection
                s = stop data collection
                z = zero out the encoder position
                p = aquire the encoder position
                d = aquire the change in encoder position
                r = sets the desired RPM of the motor
                w = send a user desired KP value
                c = Smaples and sends a CSV file with the requested speeds and times
                n = exit out and stop the motor
            A graph will be made using the collected data.
            
@author     Mathis Weyrich            
@Date       Thu Feb 18 08:41:26 2021
@copyright  License Info
"""
import pandas as pd
from matplotlib import pyplot # To make the plot
import serial,keyboard, csv,time # Keyboard = to call when you press buttons. CSV = to make a csv.doc
from array import array
## @brief The serial port initializes the serial port
# @details Change the COM port based on what USB port is used in the computer..3

ser = serial.Serial(port='COM5',baudrate=115273,timeout=1)
usr = 'yes'
state = -1
init = 1
J = 0
parsed_val_pos = 0
parsed_val_speed = 0

# def sendChar():
#     inv = input('Give me a character: ')
#     ser.write(str(inv).encode('ascii'))
#     myval = ser.readline().decode('ascii')
#     return myval

# for n in range(1):
#     print(sendChar())

passcode = 0
ready = False
finished = False 
last_key= None
inv = 0

def kb_cb(key): 
    """ Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name # Makes a variable equal the key name
    


while usr == 'yes':
    if init == 1:
        keyboard.on_release_key("S", callback=kb_cb)
        keyboard.on_release_key("g", callback=kb_cb)
        keyboard.on_release_key("z", callback=kb_cb)
        keyboard.on_release_key("d", callback=kb_cb)
        keyboard.on_release_key("p", callback=kb_cb)
        keyboard.on_release_key("r", callback=kb_cb) # Will be used to set the desired RPM of the motor
        keyboard.on_release_key("w", callback=kb_cb) # Resend a KP value
        keyboard.on_release_key("c", callback=kb_cb) # Resample and send a CSV file
        keyboard.on_release_key("n", callback=kb_cb) # All the keys that have meanings
        init = 0
    if state == -1: # A pre-state that gives all the possible instructions
        print('To acquire a data recording type in "G".\nTo Get the position of the motor type "P".\nTo get the delta in the position type "D".\nTo zero the position of the motor type "Z"\nTo set the RPM of the motor press "R"\nTo set the kp value press "W" \nTo import a CSV file press "C"\nTo quit type "n"\n')
        state = 0
    
    if state == 0: # State that writes data to the nucleo when a key is pressed
        
        try:
            if last_key is not None:
                inv = last_key
                state = 1
                ser.write(str(inv).encode('ascii'))
                last_key = None
        except KeyboardInterrupt:
            break
        
        
    if state == 1:
        if inv == 'G' or inv =='g':
            #inv = input('To acquire a data recording type in "G".')
            state = 0
            print('To stop recording data, type in "S".\nIf you do not type "S", the recording will stop after 30 seconds.')
            inv = 0
        if inv == 'S' or inv == 's':
            state = 2
        if inv == 'P' or inv == 'p':
            pos = ser.readline().decode()
            if len(pos) > 0:
                print('The position of the motor is ' + pos + '\n')
                inv = 0
                pos = 0
                state = -1
        if inv == 'D' or inv == 'd':
            delta = ser.readline().decode()
            if len(delta) >0:
                print('The delta of the motor is ' + delta + '\n')
                delta = 0
                inv = 0
                state = -1
        if inv == 'W' or inv == 'w':
            state = 7
        if inv == 'Z' or inv == 'z':
            print('The position has been zeroed\n') 
            inv = 0
            state = -1
        if inv == 'n' or inv == 'N':
            state =4
        if inv == 'r' or inv == 'R':
            state = 5
        if inv == 'c' or inv == 'C':
            state = 6
    
    
        
    if state ==2:
    
        # try:
        # # inv_2 = input('To stop recording data, type in "S".\nIf you do not type "S", the recording will stop after 30 seconds.')
        # # ser.write(str(inv_2).encode('ascii'))
        #     if last_key is not None:
        #         ser.write(str(last_key).encode('ascii'))
        #         last_key = None
        # except KeyboardInterrupt:
        #     break
        #loops = ser.readline().decode()g
        passcode = ser.readline().decode()
        passcode = passcode.strip()
        if len(passcode) != 0:
            passcode = passcode.split(',')
            state = 3
            last_key=None
            
    if state == 3:
        if int(passcode[0]) == 1:
            values = array('f',(int(passcode[1]))*[0])
            speed = array('f',(int(passcode[1]))*[0])
            times = array('f',(int(passcode[1]))*[0])
            with open('Data.csv','w') as file:
                writer = csv.writer(file, dialect='excel', lineterminator='\n')
                writer.writerow(['Times']+['Position']+['Speed'])
            
                for x in range (int(passcode[1])):
                    raw_data = ser.readline().decode()
                    rough_data = raw_data.strip()
                    if len(rough_data) != 0:
                        polished_data = rough_data.split(',')
                        values[x] = float(polished_data[1])
                        speed[x] = float(polished_data[2])
                        times[x] = float(polished_data[0])
                        
                        writer.writerow([times[x]]+[values[x]]+[speed[x]])
                
                    ready = True
        
            
        
        if ready == True:
            fig, axs = pyplot.subplots(2, sharex = True)
            fig.suptitle('Positional and Speed Data of the Motor')
            axs[0].plot(times[:-1], values[:-1])
            #pyplot.xlabel('Time')
            axs[0].set_ylabel('Data')
            
            axs[1].plot(times[:-1], speed[:-1])
            axs[1].set_xlabel('Time')
            axs[1].set_ylabel('Speed (rpm)')
            
            for x in range(len(values)):
                J = J+(1/(x+1) * ((parsed_val_speed.iloc[x]-speed[x])**2 + (parsed_val_pos.iloc[x]-values[x])**2))
            
            finished = True
            ready = False
            state = -1
            
    if state == 4:
        usr = 'No'
        
    if state == 5:
        rpm = input('Please enter the desired RPM\r\n')
        
        for x in range(len(rpm)):
            ser.write(rpm[x].encode())
        # ser.write('a'.encode())
        # ser.write(rpm.encode())
        
        state = -1
        
        
    if state == 7:
        kp = input('Please enter the desired KP\r\n')
        
        ser.write(kp.encode())
        
        state = -1
        
    if state == 6:
        keyboard.unhook_all()
        file_name = input('Enter the file name that you wish to reference\r\n')
        # with open(file_name, newline='') as csvfile:
        #     datareader = csv.reader(csvfile, dialect='excel', delimiter='')
        read_file = pd.read_csv (r'\Users\mathi\Desktop\Mathis\College\Mechatronics\Labs\Final Project\Week 4/'+file_name)
        row = read_file.shape
        parsed_val_speed = array('f',round(row[0]/50)*[0])
        parsed_val_time = array('f',round(row[0]/50)*[0])
        parsed_val_speed_rough = read_file['300'].iloc[:]
        parsed_val_pos_rough = read_file['0.1'].iloc[:]
        parsed_val_time_rough = read_file['0'].iloc[:]
        parsed_val_speed = round(parsed_val_speed_rough[0:len(parsed_val_speed_rough):50],3)
        parsed_val_pos = round(parsed_val_pos_rough[0:len(parsed_val_pos_rough):50],3)
        parsed_val_time = round(parsed_val_time_rough[0:len(parsed_val_time_rough):50],3)
        ser.write('{:}\r\n'.format(len(parsed_val_time)).encode())
        # for x in range(len(parsed_val_time)):
        #     #ser.write('{:}\r\n'.format(parsed_val_time.iloc[x]).encode())
        #     ser.write('{:}\r\n'.format(parsed_val_speed.iloc[x]).encode())
        #     #time.sleep(0.0000001)
        #     print('times'+str(x))
            

        state = 2
        init = 1
        
ser.close()
keyboard.unhook_all()
                
            
# When create array, need to preallocate array('f', 3001*[0]) - makes a list of 3001 zeros
# Convert data to strings - then use write to print one line as a time as a string


# use python string format specifier
# num1 = 10
# num2 = 5
# print('{:},{:}'.format(num1,num2)) (colons are placeholders for values)
# print('{:},{:}'.format(array1[0],array2[0])) - put in a finite state or for loop

# ^^ is essentially a CSV value

# do  myuart.write('{:},{:}'.format(array1[0],array2[0])) on nucleo

# on computer, need to undo the strings and convert back to floats to be able to plot

# can do: mysplitline= myLine.split(',') - splits at the comma
# float(mysplitline[:])


# If need to remove line endings
# myline.strip() -- removes line endings


# Comp side does not need to be a finite state machine
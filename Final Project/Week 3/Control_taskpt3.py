"""
@file       Control_taskpt3.py
@brief      Is the interface between the encoder and UI on the nucleo
@details    Creates a link between the user input variables, the encoder
            driver, the motor driver, and the closed loop task. This allows for multiple tasks to be 
            run simultaneously.
            The Control Task interfaces with the UI task using a separate variable
            file called shares.py. 
@author     Mathis Weyrich
@date       Sat Mar  6 15:20:06 2021
@copyright  License Info    
"""


import utime,math as m
import pyenc,shares,MotorDriver
from controllerdriver import ClosedLoop
from shares import *
from pyb import Pin

class Control_task:
    
    def __init__(self):

        
        self.enc = pyenc.pyenc(Pin.board.PB6,Pin.board.PB7,4,0,16384)
        self.moe = MotorDriver.MotorDriver(Pin.cpu.A15, Pin.cpu.B4, Pin.cpu.B5, 3) # Activating the motor that pairs with timer 4 (closest to power)
        
        self.mycontroller = ClosedLoop()
        self.cor = 0
        self.state = 0
        self.loop = 1
        self.Mstate = 0
        self.ms = 0
        
        self.tpr = 4000
        
            
    def run(self):
        
        self.enc.update()
        self.mycontroller.set_Kp(shares.kp)
        if self.state == 0:
            
            
            if shares.User_Input == 71 or shares.User_Input == 103:
                self.state = 1# Getting the Data
                self.Tstart = utime.ticks_ms()
                self.enc.set_position(0)
                print(self.enc.get_position())
                shares.User_Input = 0
            # if shares.User_Input == 83 or shares.User_Input == 115:
            #     self.state = 2 # Sending a preliminary data packet to tell the length of the data to the computer side
            #     shares.User_Input = 0
            if shares.User_Input == 122 or shares.User_Input == 90: #Z
                self.state = 3 # Setting The position to 0
                shares.User_Input = 0
            if shares.User_Input == 112 or shares.User_Input == 80: #P
                self.state = 4 # Print encoder position
                shares.User_Input = 0
            if shares.User_Input == 100 or shares.User_Input == 68: #d
                self.state = 5 # Print delta
                shares.User_Input = 0
            if shares.User_Input == 119 or shares.User_Input == 87: #W        
                shares.kp_get = True
                shares.User_Input = 0
            if shares.User_Input == 114 or shares.User_Input == 82: # r
                self.Mstate = 1
                shares.rpm_get = True
                shares.User_Input = 0
                self.moe.enable()
                self.enc.set_position(0)
                self.ms = 2
            if shares.User_Input == 110 or shares.User_Input == 78: #n
                self.moe.disable()
                shares.User_Input = 0
                self.Mstate = 0
               
    
        if self.state == 1:  
            # if self.myuart.any() != 0:
            #     self.val = self.myuart.readchar()
            #     print('reading')
                             
            if utime.ticks_diff(utime.ticks_ms(),self.Tstart) >= 30001:#0:
                self.state = 2
                #self.myuart.write(str(self.loop))
                print('timed out')
                
            elif shares.User_Input == 83 or shares.User_Input==115:
                self.state = 2
                print('Manually changed')
                
            elif shares.User_Input == 119 or shares.User_Input == 87: #W        
                shares.kp_get = True
                shares.User_Input = 0
                
            else:
                if utime.ticks_diff(utime.ticks_ms(), self.Tstart) >= 200*self.loop:
                    print('position data ',utime.ticks_diff(utime.ticks_ms(), self.Tstart),self.enc.get_position(),self.enc.get_delta(),shares.act_rpm)
                    shares.pos.append(self.enc.get_position())
                    shares.speed.append(shares.act_rpm)
                    shares.tims.append(utime.ticks_diff(utime.ticks_ms(), self.Tstart)/1000)
                    self.loop += 1
                    
    

        if self.state == 2:
            print('state 2')
            shares.loop = self.loop
            shares.Data_send = True
            self.loop = 0
            self.state = 0
            
        if self.state == 3:
            print('state 3')
            self.enc.set_position(0)
            self.state = 0
            
        if self.state == 4:
            print('state 4,' + str(self.enc.get_position()))
            shares.position = (1,self.enc.get_position())
            self.state = 0
            
        if self.state == 5:
            print('state 5,' + str(self.enc.get_delta()))
            shares.delta = (1,self.enc.get_delta())
            self.state = 0
            
        if self.Mstate == 1:
            if shares.des_rpm != 0:
                self.MotorStabilizer()
            
           
            
                
    def MotorStabilizer(self):
         if self.ms == 1 : # Get Velocity RPM
            self.starttime = utime.ticks_us()
            self.enc.update()
            self.delta = self.enc.get_delta()
            self.tdiff = utime.ticks_diff(utime.ticks_us(),self.starttime)
            self.position = self.enc.get_position()
            # print('This is the position', self.position)
            if self.tdiff > 0:
                shares.act_rpm = ((self.delta/self.tpr)/(self.tdiff*4))*60000000 # RPM
            elif self.tdiff == 0:
                pass
            self.ms = 2
            # print(shares.record)
            # if shares.record == True: # Only saves these values if record command is true 
            #     shares.velocity.append(self.velocity)
            #     shares.times.append(utime.ticks_ms())
            # print('This is RPM',self.velocity)
            # print('This is change in ticks', self.delta)
        
         elif self.ms == 2: # Updates From Controller
            # print('This is the actual RPM ',shares.act_rpm)
            self.pwm = self.mycontroller.update(shares.act_rpm,shares.des_rpm,shares.kp)
            #shares.kp = self.mycontroller.get_Kp()
            self.ms = 3
            
        
         elif self.ms == 3 : # Updates Motor Duty Cycle
            self.moe.set_duty(self.pwm)
            self.ms =1
         #    if shares.record == False:
         #        self.ms = 1
         #    if self.reset == True:
         #        self.tstart = utime.ticks_ms()
         #        self.reset = False 
         #        self.ms = 4
         #    if shares.record == True:
         #        self.ms = 4
            
         # elif self.state == 4: # Writes positional data to shares iterating over a set time 
         #    # Going to need time handling logic here
         #    # print('In state 4 of Con Task')
         #    if abs(utime.ticks_diff(self.tstart, utime.ticks_ms())) > 100:
         #        # print('Appending Data')
         #        shares.times.append(self.starttime)
         #        shares.velocity.append(self.velocity)
         #        self.tstart = utime.ticks_ms()
         #        self.reset = True 
         #    # Once the time is complete change shares.done to true and shares.record to false
         #    self.state = 1
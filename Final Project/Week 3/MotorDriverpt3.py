"""
@file       MotorDriverpt3.py
@brief      A driver to run the motor at a specific PWM
@author     Mathis Weyrich
@date       Thu Mar  4 09:06:30 2021
"""
from pyb import Timer, Pin, delay
# Pull the nSleep pin high, then the IN1 low and IN2 high to move in one direction.
# IN2 low and IN1 high will move other direction

#Should be 4000 ticks per revolution


# sleep = Pin(Pin.cpu.A15,mode=Pin.OUT_PP, value=1)

# # IN1/IN2 - TIM3
# tim3 = Timer(3, freq = 2000)
# m1 = tim3.channel(1,Timer.PWM, pin=Pin.cpu.B4)
# m2 = tim3.channel(2,Timer.PWM, pin=Pin.cpu.B5)
# m1.pulse_width_percent(50)
# m2.pulse_width_percent(0)

# delay(2000)

# m1.pulse_width_percent(0)
# m2.pulse_width_percent(0)

# delay(2000)

# m1.pulse_width_percent(0)
# m2.pulse_width_percent(50)

# delay(2000)

# m1.pulse_width_percent(0)
# m2.pulse_width_percent(0)



class MotorDriver:
    ''' This class implements a motor driver for the8ME405 board. '''
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
         ''' Creates a motor driver by initializing GPIO
         pins and turning the motor off for safety.
         @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
         @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
         @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
         @param timer        A pyb.Timer object to use for PWM generation on
         IN1_pin and IN2_pin. '''
         self.nSleep_pin = nSLEEP_pin
         self.IN1_pin = IN1_pin
         self.IN2_pin = IN2_pin
         self.timer = timer
         
         self.Tim = Timer(self.timer, freq = 2000)
         self.motor1 = self.Tim.channel(1, Timer.PWM, pin = self.IN1_pin)
         self.motor2 = self.Tim.channel(2, Timer.PWM, pin = self.IN2_pin)
         print ('Creating a motor driver')
    def enable(self):
        print ('Enabling Motor')
        self.nSleep = Pin(self.nSleep_pin,mode=Pin.OUT_PP,value=1)
    def disable(self):
        print ('Disabling Motor')
        self.nSleep = Pin(self.nSleep_pin,mode=Pin.OUT_PP,value=0)
    def set_duty(self, duty):
                ''' This method sets the duty cycle to be sent
                to the motor to the given level. Positive values
                cause effort in one direction, negative values
                in the opposite direction.
                @param duty A signed integer holding the duty
                cycle of the PWM signal sent to the motor '''
                self.duty = duty
                if self.duty > 0:
                   self.motor1.pulse_width_percent(duty)
                   self.motor2.pulse_width_percent(0)
                if self.duty < 0:
                   self.motor1.pulse_width_percent(0)
                   self.motor2.pulse_width_percent(duty * -1)
if __name__ =='__main__':
# Adjust the following code to write a test program for your motor class. Any
# code within the if __name__ == '__main__' block will only run when the
# script is executed as a standalone program. If the script is imported as
# a module the code block will not run.
# Create the pin objects used for interfacing with the motor driver
      pin_nSLEEP  = Pin.cpu.A15
      pin_IN1     = Pin.cpu.B4
      pin_IN2     = Pin.cpu.B5
# Create the timer object used for PWM generation
      tim     = 3
# Create a motor object passing in the pins and timer
      moe     = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)
# Enable the motor driver
      moe.enable()
# Set the duty cycle to 10 percent
      moe.set_duty(10)
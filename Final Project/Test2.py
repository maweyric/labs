# -*- coding: utf-8 -*-
"""
Created on Wed Feb 24 22:02:02 2021

@author: mathi
"""
import keyboardgGSqwertyuisgsgsgsgsgsgsgsgssssgggsssSGS
last_key = None
state = 1
inv = 'g'

def kb_cb(key):
    """ Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name

# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("S", callback=kb_cb)
keyboard.on_release_key("G", callback=kb_cb)

# Run this loop forever, or at least until someone presses control-C
while True:
    try:
    # inv_2 = input('To stop recording data, type in "S".\nIf you do not type "S", the recording will stop after 30 seconds.')
    # ser.write(str(inv_2).encode('ascii'))
        
        if last_key is not None:
            print(last_key)
    except KeyboardInterrupt:
        break

# Turn off the callbacks so next time we run things behave as expected
keyboard.unhook_all()
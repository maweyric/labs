# -*- coding: utf-8 -*-
"""
Created on Mon Mar  8 12:13:08 2021

@author: matth
"""

import utime
from controllerdriver import ClosedLoop
from encoderdriver import Encoder 
from motordriver import MotorDriver
from pyb import Timer, Pin




class ControllerTask:
    S0_INIT = 0
    S1_Update = 1
    S2_Send = 2

    
    def __init__(self,newkp,speedreq):
        TIM3 = Timer(3, freq = 20000)
        pin_nSLEEP = Pin(Pin.cpu.A15, mode=Pin.OUT_PP, value=0)
        pin_IN1    = TIM3.channel(1, mode=Timer.PWM, pin=Pin.cpu.B4)
        pin_IN2    = TIM3.channel(2, mode=Timer.PWM, pin=Pin.cpu.B5)
        self.myencoder = Encoder(Pin.cpu.B6,Pin.cpu.B7)
        self.moe = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, TIM3)
        self.speedreq = speedreq
        self.mycontroller = ClosedLoop()
        self.newkp = newkp
        self.pwm = 0
        self.samplesize = 20 #Sample to Take
        self.tpr = 4000 # Ticks Per Revolution 
        self.state = 1
        self.moe.enable()
        self.tdiff = 0
        self.mycontroller.set_Kp(self.newkp)
        self.velocity = 0
        
    def run(self): # Runs Controller Task, will be called from boss.py
        if self.state == 1 : # Get Velocity RPM
            self.starttime = utime.ticks_us()
            self.myencoder.update()
            self.delta = self.myencoder.get_delta()
            self.tdiff = utime.ticks_diff(utime.ticks_us(),self.starttime)
            self.position = self.myencoder.get_position()
            print('This is the position', self.position)
            if self.tdiff > 0:
                self.velocity = ((self.delta/self.tpr)/(self.tdiff))*600000 # RPM
            elif self.tdiff == 0:
                pass
            self.state = 2
            print('This is RPM',self.velocity)
            print('This is change in ticks', self.delta)
        
        elif self.state == 2:
            self.pwm = self.mycontroller.update(self.velocity,self.speedreq)
            self.state = 3
            
        
        elif self.state == 3 :
            self.moe.set_duty(self.pwm)
            self.state = 1
        
        
cont = ControllerTask(0.001,50)

while True:
    cont.run()
    

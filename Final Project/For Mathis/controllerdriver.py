# -*- coding: utf-8 -*-
"""
Created on Thu Mar  4 10:19:24 2021

@author: matth
"""


class ClosedLoop:
    
    def __init__(self):
        self.pmax = 99.5 # Max PWM
        self.pmin = -99.5 # Min PWM 
        self.pwm = 30 # Current PWM
        self.kp = 0 # Gain Constant
        self.deltapwm = 0 # Last Change in PWM
        self.newpwm = 30
       
    
    def update(self,speedcurrent,speedrequest):
        self.speedreq = speedrequest ## Request this from controller task 
        self.speedcurrent = speedcurrent
        self.deltapwm = self.kp*(self.speedreq - self.speedcurrent)
        self.newpwm = self.newpwm + self.deltapwm
        print('This is newpwm',self.newpwm)
        if self.newpwm >= self.pmin and self.newpwm <= self.pmax:
            self.pwm = self.pwm + self.deltapwm
            print('Changing Speed')
            print('This is delta pwm', self.deltapwm)
            print('This is PWM', self.pwm)
            
            return self.pwm
        else:
            self.pwm = self.pmax
            print('Max Or Minimum Power Achieved')
            ## Will throw error here because calling function expecting interger
            
       
    
    def get_Kp(self): # Sends out KP to place calling it
        return self.kp
    
    def set_Kp(self, newkp): # Sets kp to current kp from controller task
        self.kp = newkp
        pass
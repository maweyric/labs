"""
@file       SerialTest.py
@brief      The computer side of a direction communication link 
@details    The computer initializes a serial port with the nucleo for direct
            communication. The computer sends a specific string based on a 
            keyboard input. Once the read code is sent, the computer waits for 
            the passcode, at which points it creates an appropriately sized 
            array and ports the data from the nucleo into a CSV file.
            
@author     Mathis Weyrich            
@Date       Thu Feb 18 08:44:11 2021
@copyright  License Info
"""
from matplotlib import pyplot
import serial,keyboard, csv
from array import array
ser = serial.Serial(port='COM5',baudrate=115273,timeout=1)
usr = 'yes'
state = -1

# def sendChar():
#     inv = input('Give me a character: ')
#     ser.write(str(inv).encode('ascii'))
#     myval = ser.readline().decode('ascii')
#     return myval

# for n in range(1):
#     print(sendChar())
values = array('f',3001*[0])
times = array('f',3001*[0])
passcode = 0
ready = False
finished = False 
last_key= None
inv = 0

def kb_cb(key):
    """ Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name
    
keyboard.on_release_key("S", callback=kb_cb)
keyboard.on_release_key("g", callback=kb_cb)

while usr == 'yes':
    if state == -1:
        print('To acquire a data recording type in "G".')
        state = 0
    
    if state == 0:
        
        try:
            if last_key is not None:
                inv = last_key
        except KeyboardInterrupt:
            break
        if inv == 'G' or inv =='g':
            #inv = input('To acquire a data recording type in "G".')
            ser.write(str(inv).encode('ascii'))
            state = 1
            print('To stop recording data, type in "S".\nIf you do not type "S", the recording will stop after 30 seconds.')
        
    if state ==1:
    
        try:
        # inv_2 = input('To stop recording data, type in "S".\nIf you do not type "S", the recording will stop after 30 seconds.')
        # ser.write(str(inv_2).encode('ascii'))
            if last_key is not None:
                ser.write(str(last_key).encode('ascii'))
                inv = 0
        except KeyboardInterrupt:
            break
        #loops = ser.readline().decode()g
        passcode = ser.readline().decode()
        passcode = passcode.strip()
        
        if len(passcode) != 0:
            passcode = passcode.split(',')
            state = 2
            last_key=None
            
        if state == 2:
            if int(passcode[0]) == 1:
                with open('Data.csv','w') as file:
                    writer = csv.writer(file, dialect='excel', lineterminator='\n')
                    writer.writerow(['Times']+['Data'])
                
                    for x in range (int(passcode[1])):
                        raw_data = ser.readline().decode()
                        rough_data = raw_data.strip()
                        if len(rough_data) != 0:
                            polished_data = rough_data.split(',')
                            values[x] = float(polished_data[1])
                            times[x] = float(polished_data[0])
                            
                            writer.writerow([times[x]]+[values[x]])
                    
                        ready = True
            
                
            
            if ready == True:
                pyplot.figure()
                pyplot.plot(times, values)
                pyplot.xlabel('Time')
                pyplot.ylabel('Data')
                
                finished = True
                ready = False
                
            if finished == True:
                usr = input('Would you like to collect more data?')
                finished = False
                state = -1

ser.close()
keyboard.unhook_all()
                
            
# When create array, need to preallocate array('f', 3001*[0]) - makes a list of 3001 zeros
# Convert data to strings - then use write to print one line as a time as a string


# use python string format specifier
# num1 = 10
# num2 = 5
# print('{:},{:}'.format(num1,num2)) (colons are placeholders for values)
# print('{:},{:}'.format(array1[0],array2[0])) - put in a finite state or for loop

# ^^ is essentially a CSV value

# do  myuart.write('{:},{:}'.format(array1[0],array2[0])) on nucleo

# on computer, need to undo the strings and convert back to floats to be able to plot

# can do: mysplitline= myLine.split(',') - splits at the comma
# float(mysplitline[:])


# If need to remove line endings
# myline.strip() -- removes line endings


# Comp side does not need to be a finite state machine
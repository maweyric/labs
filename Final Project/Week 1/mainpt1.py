"""
@file       mainpt1.py
@brief      The nucleo side of a direction communication link 
@details    The nucleo recieves commands from the computer that prompts it to 
            start taking data or stop taking data based on user input. This
            data is then sent to the computer for further processing.
@author     Mathis Weyrich            
@Date       Thu Feb 18 08:44:11 2021
@copyright  License Info
"""
import utime, math as m
from array import array
from pyb import UART
import pyb

pyb.repl_uart(None)
# myuart = UART(2)
# while True:
#     if myuart.any() != 0:
#         val = myuart.readchar()
        
#         if val == 104:
#             myuart.write(str(val))
#         else:
#             myuart.write(str(val))

class datacoms:

    
    def __init__(self):
        '''
        @brief  Initializing variables and the communication port
        '''

        self.myuart = UART(2) # Reference to the physical port that will work
        
        self.data_a = array('f', 301*[0])
        self.tims_a = array('f', 301*[0])
        self.data = list()
        self.tims = list()
        self.state = 0
        self.loop = 1
        self.val = 0
            
    def run(self):
        '''
        @brief   Runs a finite state machine that creates and sends data
        @details The initial state waits for an input signal from the computer
                 side and sets the following state based on the input. An input
                 requests data to be read for up to 30 seconds or whenever the 
                 user sends another input. Following the end of data collection
                 the nucleo sends an initial passcode to the computer with the
                 amount of data to be sent. Following that, the data is sent in
                 a csv style string.

        '''
        
        if self.state == 0:
            
            if self.myuart.any() != 0:
                self.val = self.myuart.readchar()
                
            if self.val == 71 or self.val == 103:
                self.state = 1
                self.Tstart = utime.ticks_ms()
                self.val = 0
            if self.val == 84 or self.val == 116:
                self.state = 3
                self.val = 0
                
               
    
        if self.state == 1:
            
            
            if self.myuart.any() != 0:
                self.val = self.myuart.readchar()
                print('reading')
                             
            if utime.ticks_diff(utime.ticks_ms(),self.Tstart) >= 30001:#0:
                self.state = 3
                #self.myuart.write(str(self.loop))
                print('timed out')
                
            elif self.val == 83 or self.val==115:
                self.state = 3
                
                print('Manually changed')
                
            else:
                if utime.ticks_diff(utime.ticks_ms(), self.Tstart) >= 100*self.loop:
                    print(utime.ticks_diff(utime.ticks_ms(), self.Tstart))
                    self.data.append(m.exp(-0.1*utime.ticks_diff(utime.ticks_ms(), self.Tstart)/1000)*m.sin((2/3) * m.pi * utime.ticks_diff(utime.ticks_ms(), self.Tstart)/1000))
                    self.tims.append(utime.ticks_diff(utime.ticks_ms(), self.Tstart)/1000)
                    self.loop += 1
                    print('looping')
    
        if self.state == 2:
            
            
            for x in range (len(self.data)):
                self.myuart.write('{:},{:}\r\n'.format(self.tims[x],self.data[x]))
                
                print('sending data' + str(x))
                
            self.state = 0
            self.val = 0
            print('Done')
    
        if self.state == 3:
            print('state 3')
            self.myuart.write('{:},{:}\r\n'.format(1,self.loop))
            self.state = 2
        
        
nucleocomms = datacoms()
while True:
    nucleocomms.run()

# Virtual com port if using the native usb - use USB_VCP instead of UART
# Top = UART
# Bottom = USB_VCP
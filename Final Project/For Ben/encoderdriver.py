# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 16:04:48 2021

@author: matth
"""

import pyb
from pyb import Timer


class Encoder:
    
    
    def __init__(self,CH1,CH2,timer): # Need to add timer for second motor.
        self.CH1 = CH1
        self.CH2 = CH2
        self.period = 65535
        self.timer = timer
        
        self.Timer = Timer(self.timer, period=self.period, prescaler=0)
        self.Timer.channel(1, mode=pyb.Timer.ENC_AB, pin=self.CH1)
        self.Timer.channel(2, mode=pyb.Timer.ENC_AB, pin=self.CH2)
        
        
        self.position = 0
        self.prevnum = 0
        self.currnum = 0
        
        self.delta = 0
       
        
        
    
    def update(self):
        self.prevnum  = self.currnum
        self.currnum = self.Timer.counter()
        self.delta = self.currnum - self.prevnum
        
        if abs(self.delta) > self.period/2 and self.delta > 0:
            self.delta = self.period - self.delta
        elif abs(self.delta) > self.period/2 and self.delta < 0:
            self.delta = self.period + self.delta
        
        self.position = self.position + self.delta
        
        
            
        
    def get_position(self):
        return self.position 

    
    def set_position(self):
        self.prevnum = self.Timer.counter()
        self.position = 0
        
    
    def get_delta(self):
        return (self.delta)
        
    
    


    
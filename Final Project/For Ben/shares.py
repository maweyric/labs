# -*- coding: utf-8 -*-
"""
Created on Mon Mar  8 11:36:48 2021

@author: matth
"""

from array import array 

times = array('f',1*[0]) #Times at which the position was recorded, subtract out first value from all values)
velocity = array('f',1*[0]) # RPM at the various times
start = False # Indicates if motors should start
record = False  # Indicates if time and velocity should be recorded to shares files
done = False # Indicates if the 30 seconds are up
newkp = 0.001
speedreq = 800
currentkp = 0.001


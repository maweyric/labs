# -*- coding: utf-8 -*-
"""
Created on Mon Mar  8 12:13:54 2021

@author: matth
"""
from Con_Task import ControllerTask
from UI_Task import tasks

task1 = tasks()
cont = ControllerTask()

while True:
    cont.run()
    task1.run()
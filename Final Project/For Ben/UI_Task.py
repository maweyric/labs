# -*- coding: utf-8 -*-
"""
Created on Mon Mar  8 12:13:32 2021

@author: matth
"""

import utime 
from array import array
from pyb import UART, Pin
import pyb
import shares
from shares import *

pyb.repl_uart(None)



class tasks:
    
    ## State Constants
    S0_INIT = 0
    S1_DataRec = 1
    S2_DataSend = 2
    
    def __init__(self):
        self.myuart = UART(2) #UART is a class not an object, so no need for self.
        self.state = 0
        self.usrinput = 0
        self.newinput = 0
        self.times = array('f',1*[0])
        self.values = array('f',1*[0])
        self.short = False
        self.run1 = False
        self.n = 0
        self.i = 1
        self.currenttime = 0
        self.waittime = 0
        self.newk = False
        self.newkp = 0
        self.currentkp = shares.currentkp
        
        
    def run(self):
         
         if self.state == self.S0_INIT:
            self.t0 = utime.ticks_ms
            self.state = 1
            print('In state 1')
        
         elif self.state == 1:
             if  self.myuart.any() != 0:
                 self.newinput = self.myuart.readchar()
                 if self.newinput == 103 or self.newinput ==  115 or self.newinput ==  100 or self.newinput ==  112 or self.newinput ==  122:
                     self.usrinput = self.newinput
                     self.time = utime.ticks_ms()
                     self.i = 0 # For state 2 uses on second run
                 if self.newk == True: # sets up recieve for the new kp
                     self.newkp = self.myuart.readchar()
                     shares.newkp = self.newkp
                     pass
                    
            
             elif self.usrinput == 112: #p indicates new kp coming 
                  self.newk = True
                  
             elif self.usrinput == 100: #d # Indiciates print out the current kp
                  self.currentkp = shares.currentkp
                  self.state = 2
                  self.currentkp = True
                  self.short = True 
                  pass
             
             
             elif self.usrinput == 4: # change to m or something 
                 pass
             
            
    
             elif self.usrinput == 103: #g waits until shares sends complete then wiretes new data to array 
                  if utime.ticks_diff(utime.ticks_ms(), self.time) <= 30000: # sets time to check each value for sending
                      shares.record = True 
                      
                      
                      
                      
                  elif utime.ticks_diff(utime.ticks_ms(), self.time) > 30000:
                      self.times = shares.times
                      self.velocity = shares.velocity
                      shares.record = False
                      self.state = 3
             
             elif self.usrinput == 115: #s
                  self.state = 3
                  pass
              
             else: 
                 pass
         
         elif self.state == 2:
             if self.short == True :
                 if self.run1 == False:
                     print('Sending Info Code')
                     self.mystring = ('{:}, {:}\r\n'.format(2,2))
                     self.myuart.write(self.mystring)
                     self.run1 = True
                 elif self.currentkp == True:  
                     print('Sending Current Kp Info')
                     self.mystring = ('{:}, {:}\r\n'.format(self.encoderposition, 0)) # Zero tells interface this is encoder position
                     self.myuart.write(self.mystring)
                     self.short = False
                     self.state = 4
                 elif self.encdel == True:
                     print('Sending Encoder Delta Short Info')
                     self.mystring = ('{:}, {:}\r\n'.format(self.encoderdelta, 1)) # One tells interface this is delta 
                     self.myuart.write(self.mystring)
                     self.short = False
                     self.state = 4
             
             
         elif self.state == 3:
             if self.i < len(self.velocity):
                if self.run1 == False:
                    print('Sending Info Code')
                    self.mystring = ('{:}, {:}\r\n'.format(len(self.velocity), len(self.times)))
                    self.myuart.write(self.mystring)
                    self.run1 = True
                else:
                    print('Sending Values')
                    self.mystring = ('{:}, {:}\r\n'.format(self.times[self.i], self.velocity[self.i]))
                    self.myuart.write(self.mystring)
                    self.i = self.i + 1
                    print(self.i)
                    print('This is length of the data to be sent on nucleo',len(self.velocity))
             
             elif self.i >= len(self.velocity):
                    print('Moving to state 4')
                    self.state = 4
                    
         elif self.state == 4 :
                self.state = 1
                self.newinput = 0
                self.usrinput = 0
                self.run1 = False
                self.currentkp = False
                self.encdel = False
                self.times = array('f',1*[0])
                self.velocity = array('f',1*[0])
                 
                 
# task1 = tasks()

# while True:
    
#     task1.run()
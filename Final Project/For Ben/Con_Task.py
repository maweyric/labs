# -*- coding: utf-8 -*-
"""
Created on Mon Mar  8 12:13:08 2021

@author: matth
"""

import utime
from controllerdriver import ClosedLoop
from encoderdriver import Encoder 
from motordriver import MotorDriver
from pyb import Timer, Pin
from pyb import UART, Pin
import pyb
pyb.repl_uart(None)
import shares
from shares import*






class ControllerTask:
    S0_INIT = 0
    S1_Update = 1
    S2_Send = 2

    
    def __init__(self):
        TIM3 = Timer(3, freq = 20000)
        pin_nSLEEP = Pin(Pin.cpu.A15, mode=Pin.OUT_PP, value=0)
        pin_IN1   = TIM3.channel(1, mode=Timer.PWM, pin=Pin.cpu.B4)
        pin_IN2   = TIM3.channel(2, mode=Timer.PWM, pin=Pin.cpu.B5)
        pin_IN3   = TIM3.channel(3, mode=Timer.PWM, pin=Pin.cpu.B0)
        pin_IN4   = TIM3.channel(4, mode=Timer.PWM, pin=Pin.cpu.B1)
        self.myencoder1= Encoder(Pin.cpu.B6,Pin.cpu.B7,4)
        self.myencoder2 = Encoder(Pin.cpu.C6,Pin.cpu.C7,8)
        self.moe1 = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, TIM3)
        self.moe2 = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, TIM3)
        self.speedreq = shares.speedreq
        self.mycontroller = ClosedLoop()
        self.newkp = shares.newkp
        self.pwm = 0
        self.samplesize = 20 #Sample to Take
        self.tpr = 4000 # Ticks Per Revolution 
        self.state = 1
        self.moe2.enable()
        self.tdiff = 0
        self.mycontroller.set_Kp(self.newkp)
        self.velocity = 0
        self.tstart = utime.ticks_ms()
        self.reset = False
        
    def run(self): # Runs Controller Task, will be called from boss.py
        if self.state == 1 : # Get Velocity RPM
            self.starttime = utime.ticks_us()
            self.myencoder2.update()
            self.delta = self.myencoder2.get_delta()
            self.tdiff = utime.ticks_diff(utime.ticks_us(),self.starttime)
            self.position = self.myencoder2.get_position()
            # print('This is the position', self.position)
            if self.tdiff > 0:
                self.velocity = ((self.delta/self.tpr)/(self.tdiff))*6000000 # RPM
            elif self.tdiff == 0:
                pass
            self.state = 2
            # print(shares.record)
            # if shares.record == True: # Only saves these values if record command is true 
            #     shares.velocity.append(self.velocity)
            #     shares.times.append(utime.ticks_ms())
            # print('This is RPM',self.velocity)
            # print('This is change in ticks', self.delta)
        
        elif self.state == 2: # Updates From Controller
            self.pwm = self.mycontroller.update(self.velocity,self.speedreq)
            shares.currentkp = self.mycontroller.get_Kp()
            self.state = 3
            
        
        elif self.state == 3 : # Updates Motor Duty Cycle
            self.moe2.set_duty(self.pwm)
            if shares.record == False:
                self.state = 1
            if self.reset == True:
                self.tstart = utime.ticks_ms()
                self.reset = False 
                self.state = 4
            if shares.record == True:
                self.state = 4
            
        elif self.state == 4: # Writes positional data to shares iterating over a set time 
            # Going to need time handling logic here
            # print('In state 4 of Con Task')
            if abs(utime.ticks_diff(self.tstart, utime.ticks_ms())) > 100:
                # print('Appending Data')
                shares.times.append(self.starttime)
                shares.velocity.append(self.velocity)
                self.tstart = utime.ticks_ms()
                self.reset = True 
            # Once the time is complete change shares.done to true and shares.record to false
            self.state = 1
# cont = ControllerTask(0.0001,1000)

# while True:
#     cont.run()
    

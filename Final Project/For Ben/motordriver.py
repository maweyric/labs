# -*- coding: utf-8 -*-
"""
Created on Thu Mar  4 08:26:29 2021

@author: matth
"""
# from pyb import Timer, Pin, delay
# TIM3 = Timer(3, freq = 20000)

class MotorDriver:
    
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
        print('Creating motor driver')
        self.IN1Pin = IN1_pin
        self.IN2Pin = IN2_pin
        self.nSLEEPpin = nSLEEP_pin
        self.timer = timer
    
    def enable (self):
        
        print('Enabling Motor')
        self.nSLEEPpin.high()
    
    
    def disable (self):
        print('Disabling Motor')
        self.nSLEEPpin.low()
    
    
    def set_duty (self, duty):
        self.duty = duty 
        if self.duty > 0 :
            self.IN1Pin.pulse_width_percent(self.duty)
            self.IN2Pin.pulse_width_percent(0)
        elif self.duty < 0 :
            self.IN1Pin.pulse_width_percent(0)
            self.IN2Pin.pulse_width_percent(self.duty)
        elif self.duty == 0 :
            self.IN1Pin.pulse_width_percent(0)
            self.IN2Pin.pulse_width_percent(0)
        
        
        
# if __name__ == '__main__':
    
#     pin_nSLEEP = Pin(Pin.cpu.A15, mode=Pin.OUT_PP, value=0)
#     pin_IN1    = TIM3.channel(1, mode=Timer.PWM, pin=Pin.cpu.B4)
#     pin_IN2    = TIM3.channel(2, mode=Timer.PWM, pin=Pin.cpu.B5)
    
    
#     time       = TIM3
    
#     moe        = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, time)
    
#     moe.enable()
    
#     moe.set_duty(50)
    
#     delay (2000)
    
#     moe.set_duty(0)
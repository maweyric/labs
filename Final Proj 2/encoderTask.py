"""@file encoderTask.py
@brief Runs the encoders and gets data to send to shares
@details This task samples the encoder driver at a scheduled rate and converts the encoder data to both angle and angular velocity. The task also converts
this data to a plate angular velocity. This data is then put into the shares file. 
@author: Daniel Freeman & Mathis Weyrich
"""

import pyb, shares, utime, math, task_share, cotask, print_task
from encoderDriver import encoderDriver

def encoderTask():
    '''@brief Function to run encoders 
    @details Creates encoder object and samples both encoders at a given rate. Converts tick reading to plate angular velocity in rad/s and plate angle in radians
    '''
    ## Encoder 1 pin 1
    e1p1 = pyb.Pin(pyb.Pin.cpu.B6)
    ## Encoder 1 pin 2
    e1p2 = pyb.Pin(pyb.Pin.cpu.B7) 
    ## Encoder 2 pin 1
    e2p1 = pyb.Pin(pyb.Pin.cpu.C6)
    ## Encoder 2 pin 2
    e2p2 = pyb.Pin(pyb.Pin.cpu.C7) 
    ## Timer 1
    tim1 = 4 
    ## Timer 2
    tim2 = 8
    ## Object of encoder class
    encoder = encoderDriver(e1p1, e1p2, e2p1, e2p2, tim1, tim2)
    ## Start time of program, milliseconds
    start = utime.ticks_ms()
    ## Program sample rate, milliseconds
    rate = 10
    ## Radius of lever arm, mm
    rm = 60
    ## Horizontal distance from U-joint to push-rod pivot
    lp = 110
    
    while True:
        #print_task.put_bytes(b'Enc\r\n')
        if utime.ticks_diff(utime.ticks_ms(), start) > rate:
            encoder.update()
            _position = encoder.get_position()
            ## Encoder 1 measured position, in radians
            rads1 = ((_position[0])/4000)*2*math.pi  #To make sure the system is leveld, begin with the phone on as a level
            ## Encoder 2 measured position, in radians
            rads2 = ((_position[1])/4000)*2*math.pi 
            #print_task.put_bytes(b'Enc1: {:} ticks, {:} rads\r\n'.format(_position[0],rads1))
            #print_task.put_bytes(b'Enc2: {:} ticks, {:} rads\r\n'.format(_position[1],rads2))
            shares.thx.put(rads1)
            shares.thy.put(rads2)
            
            
            _delta = encoder.get_delta()
            ## Encoder 1 measured speed, in rad/s
            motorspeed1 = _delta[0]/rate
            ## Encoder 2 measured speed, in rad/s
            motorspeed2 = _delta[1]/rate
            # print_task.put_bytes(b'M1: {:} rad/s\r\n'.format(motorspeed1))
            # print_task.put_bytes(b'M2: {:} rad/s\r\n'.format(motorspeed1))
            ## Plate speed as measured by encoder 1, in rad/s
            platespeed1 = -(rm/lp)*motorspeed1
            ## Plate speed as measured by encoder 2, in rad/s
            platespeed2 = -(rm/lp)*motorspeed2
            shares.thx_d.put(platespeed1)
            shares.thy_d.put(platespeed2)
            
            if shares.zero_enc == 116:
                encoder.set_position(0,0)
                print_task.put('Encoder positions = zero')
                shares.zero_enc.put(102)
            
            start = utime.ticks_ms()
              
                
        yield(0)    # To exit out of the generator
        
enctask = cotask.Task (encoderTask, name = 'encoderTask', priority = 1, 
                  period = 35, profile = True, trace = False)
cotask.task_list.append (enctask)    
        
# Delta value can be used to get the speed if we divide by time
# The time we are dividing by would be the frequency at which we are sampling (the task period)
# The task period is set when we create the task object
# It may be better to use utime_ticks_diff
# Remember we really want the platform angle not the motor angle - look at the HW2 gear ratio assumption
# Our encoders have 4000 ticks per revolution
# Radians or degrees? It should match our gains
            
 
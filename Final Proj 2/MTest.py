# -*- coding: utf-8 -*-
"""
Created on Tue Jun  8 15:26:59 2021

@author: HP
"""
import MotorDriver, utime, pyb, shares, BNO055, encoderDriver
from pyb import I2C


drv = MotorDriver.DRV8847(pyb.Pin.cpu.A15, pyb.Pin.cpu.B2)
M1 = drv.channel(pyb.Pin.cpu.B4, pyb.Pin.cpu.B5,3, 1,2)
M2 = drv.channel(pyb.Pin.cpu.B0, pyb.Pin.cpu.B1,3, 3,4)
drv.enable()

print("Motor 1 going In positive Direction")
M1.set_level(50)
pyb.delay(100)
M1.set_level(0)
pyb.delay(100)

print("Motor 1 going In Negative Direction")
M1.set_level(-70)
pyb.delay(100)
M1.set_level(0)
pyb.delay(100)

print("Motor 2 going In positive Direction")
M2.set_level(50)
pyb.delay(100)
M2.set_level(0)
pyb.delay(100)

print("Motor 2 going In negative Direction")
M2.set_level(-70)
pyb.delay(100)
M2.set_level(0)
pyb.delay(100)
    
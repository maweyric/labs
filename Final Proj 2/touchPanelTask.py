"""@file    touchPanelTask.py
@brief      Task for collecting data from the touch panel
@details    If the ball has been located on the board (z==True), then the X, Y,
            X_d, and Y_d are added to Shares.py.
@author:    Daniel Freeman & Mathis Weyrich
"""
import touchPanelDriver, shares, cotask, print_task


def touchPanelTask():
    '''@brief   Function to run touch panel
    @details    Creates object of touch panel driver and gets x and y positions in 
                mm and send them to shares file. Also gets z position, telling us 
                if the ball is on the platform. The velocity for the first 2 runs 
                of this task is artificially set as 0 to eliminate initial velocity
                errors. In addition, if the change in position is larger than 20
                between runs, the new position is replaced by the previous position.
    '''
    ## Length of the panel in mm
    length = 186
    ## Width of the panel in mm
    width = 108
    ## Object of touchpanel driver
    touchPanel = touchPanelDriver.touchPanelDriver(length, width)
    _state = 0
    _h = 0
    
    
    while True:
        ## z value, 1 = off the board, 0 = on the board
        z = touchPanel.z_pos()
        if z == False: # this code runs through once to eliminate unnecessary print spam
            if _state == 0:
                #print_task.put_bytes(b'Ball off board\r\n')
                shares.z.put(102)
                _state = 1
            if _state == 1:
                pass
        elif z == True: # If the ball is on the board, this code runs through once to eliminate unnecessary print spam
            if _state == 1:
                #print_task.put_bytes(b'Ball on board\r\n')
                shares.z.put(116)
                _state = 0
            if _state == 0:
            
                ## x position in mm
                x = (touchPanel.x_pos())
                ## y position in mm
                y = (touchPanel.y_pos())
                
#____________________________________________________________________
# Filter to eliminate random touchpanel data greater than delta 20                
                if abs(shares.y.get()-y) > 20:
                    # y = y+((shares.y.get()-y)/(((10-abs(shares.y.get()-y))/10)*-1 + 2)) # Filtering large jumps while still taking into account a potential change in motion. (dampens the outliers)
                    # print('corY')
                    y = shares.y.get()
                if abs(shares.x.get()-x) > 20:
                    # x = x+((shares.x.get()-x)/(((10-abs(shares.x.get()-x))/10)*-1 + 2))
                    # print('corX')
                    x = shares.x.get()
                
                # print_task.put_bytes(b'X:')
                # print_task.put_bytes(b'x')
                # print_task.put_bytes(b'Y:')
                # print_task.put_bytes(b'y')
                
#____________________________________________________________________                
# To ensure we do not have random large velocities right when the ball is on the board.
                if _h < 2:
                    _x_d = 0
                    _y_d = 0
                    _h +=1
                else:
                    _x_d = ((x) - shares.x.get())/0.6 # Dividing by the period to get the approximate m/s
                    _y_d = ((y) - shares.y.get())/0.6 # Same as above
                
                
                shares.x.put((x))
                shares.y.put((y))
                shares.x_d.put(_x_d)
                shares.y_d.put(_y_d)
                
                
            
        yield(0)
        
_touchtask = cotask.Task (touchPanelTask, name = 'touchPanelTask', priority = 1, 
                         period = 25, profile = True, trace = False)
cotask.task_list.append (_touchtask)

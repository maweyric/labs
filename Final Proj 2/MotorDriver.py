"""
@file       MotorDriver.py
@brief      A driver to run the motor at a specific PWM
@author     Mathis Weyrich
@date       Thu May 13 12:26:10 2021

"""


import pyb

# For documentation: Need a write up of testing procedures (Testing code on bottom of the file), including a 
# video of it in motion. Need to find a benchmark value to overcome static friction. 

# Testing Procedures:
    # Initialized the object for both motor 1 and 2. 
    # Ran motor 1 in the forward direction for 0.5 seconds
    # Turned off motor 1 for 0.5 seconds
    # Ran motor 1 in the negative direction for 0.5 seconds
    # Turned off motor 1 for 0.5 seconds
    # Repeated for motor 2.
    
    # Run motor 1 from 50 to 0 going down by 5 to see where the motor stops
    # My Motor 1 can run if it is above 30
    # My Motor 2 is thoroughly fucked and cannot move at all

class DRV8847:
    
    def __init__ (self, nSLEEP_pin, nFAULT_pin):
        '''
        @brief Initializes the sleep pin and the fault interrupt
        @param nSLEEP_pin Selects the pin that can activate or deactivate the motor
        @param nFAULT_pin Selects the pin that will activate the fault IRQ

        '''
        self._nSleep_pin = nSLEEP_pin
        self._nFault_pin = nFAULT_pin
        
        self._nSleep = pyb.Pin(self._nSleep_pin,mode=pyb.Pin.OUT_PP)
        
        self._fault = False
        
        self._FaultInt = pyb.ExtInt(self._nFault_pin, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, self.fault_CB) 
        #Creates the IRQ associated with the fault pin tripping
    
    def enable(self):
        '''
        @brief Enables the Motor
        '''
        print ('Enabling Motor')
        if self._fault == False:
            self._FaultInt.disable()
            self._nSleep.high()
            pyb.udelay(2)
            self._FaultInt.enable()
        else:
            print("Clear Motor Fault before Enabling")
            pass
        
    
    def disable(self):
        '''
        @brief Disables the Motor
        '''
        print ('Disabling Motor')
        self._nSleep.low()
    
    def fault_CB(self, IRQ_src):
        '''
        @brief Disables the motor when the fault pin is activated and prevents immediate re-enabling
        
        '''
        self.disable()
        self._fault = True
        return 'Fault'
    
    def clear_fault(self):
        '''
        @brief Clears the fault, allowing for re-enabling of the motor

        '''
        self._fault = False
    
    def channel(self, INx_pin, INy_pin, INxy_timer, ch1, ch2):
        '''
        @brief Creates a motor object
        @param INx_pin Input your pin object that causes negative movement
        @param INy_pin Input your pin object that causes positive movement 
        @param INxy_timer Input a number relating to the timer desired
        @param ch1 Input the number corellated with the desired INx_pin and timer
        @param ch2 Input the number corellated with the desired INy_pin and timer
        @returns The created motor object

        '''
        return DRV8847_channel(INx_pin, INy_pin, INxy_timer, ch1, ch2)
    
class DRV8847_channel:
    
    def __init__(self, INx_pin, INy_pin, INxy_timer,ch1, ch2):
        '''
        @brief Initializes the timer and PWM to be able to use the motor object
        @param INx_pin Input your pin object that causes negative movement
        @param INy_pin Input your pin object that causes positive movement 
        @param INxy_timer Input a number relating to the timer desired
        @param ch1 Input the number corellated with the desired INx_pin and timer
        @param ch2 Input the number corellated with the desired INy_pin and timer

        '''
        self.INX = INx_pin
        self.INY = INy_pin
        self.ch1 = ch1
        self.ch2 = ch2
        self.INxy_tim = INxy_timer
        self.tim = pyb.Timer(self.INxy_tim, freq = 20000)
        self.mx = self.tim.channel(self.ch1, pyb.Timer.PWM, pin = self.INX)
        self.my = self.tim.channel(self.ch2, pyb.Timer.PWM, pin = self.INY)
    
    def set_level(self, level):
        '''
        @brief Sets the level of motion for the motor 
        @param level    The level is an integer based on the percent PWM desired. 
                        This value can be positive (for positive motion), negative
                        (for negative motion) or zero (for no motion).

        '''
        self.level = level
        if self.level > 0: # Positive Direction
            self.my.pulse_width_percent(self.level)
            self.mx.pulse_width_percent(0)
            #print("Positive")
        if self.level < 0: # Negative direction
            self.mx.pulse_width_percent(self.level*-1)
            self.my.pulse_width_percent(0)
            #print("Negative")
        if self.level == 0: # No movement
            self.my.pulse_width_percent(0)
            self.mx.pulse_width_percent(0)
            #print("zero")
        
    
if __name__ == "__main__":
    drv = DRV8847(pyb.Pin.cpu.A15, pyb.Pin.cpu.B2)
    M1 = drv.channel(pyb.Pin.cpu.B4, pyb.Pin.cpu.B5,3, 1,2)
    M2 = drv.channel(pyb.Pin.cpu.B0, pyb.Pin.cpu.B1,3, 3,4)
    drv.enable()
    
    print("Motor 1 going In positive Direction")
    M1.set_level(50)
    pyb.delay(500)
    M1.set_level(0)
    pyb.delay(500)
    
    print("Motor 1 going In Negative Direction")
    M1.set_level(-50)
    pyb.delay(500)
    M1.set_level(0)
    
    print("Motor 2 going In positive Direction")
    M2.set_level(50)
    pyb.delay(500)
    M2.set_level(0)
    
    print("Motor 2 going In negative Direction")
    M2.set_level(-50)
    pyb.delay(500)
    M2.set_level(0)
    
    for x in range(11):
        M1.set_level(50-x*5)
        M2.set_level(50 - x*5)
        print(50-x*5)
        pyb.delay(500)
        M1.set_level(0)
        M2.set_level(0)
        pyb.delay(500)
        
    
"""@file main.py
@brief Main program running on nucleo
@details This file runs the tasks, which run the drivers. 
Created on Thu May 27 13:57:13 2021
@author: Mathis Weyrich
"""


import task_share, cotask, print_task
import shares
import encoderTask, ControllerTask, MotorTask  ,IMUTask, UITask, touchPanelTask, dataTask
import utime, gc, pyb

print ('\033[2JTesting scheduler in cotask.py\n')
_loop = True
if __name__ == "__main__":
    gc.collect()
    print('To reset position, press "C".\nTo enable the motor type "E".\nTo set the Operating Mode of the BNO chip to IMU, type "I".\n')
       
    while _loop == True:
        try:
            cotask.task_list.pri_sched ()
            
        except KeyboardInterrupt:
            _loop = False


    # Print a table of task data and a table of shared information data
    print ('\n' + str (cotask.task_list) + '\n')
    print (task_share.show_all ())
    print ('\r\n')
        



"""@file    dataTask.py
@brief      File for collecting and sending data
@details    This file is used to collect data from our drivers including: touch panel position, 
            PWM, motor angle and times. A CSV file is generated on the nucleo with all the data 
            and corresponding timestamps.
@author:    Daniel Freeman & Mathis Weyrich
"""

import shares, cotask, print_task
import array
import utime, uos





def dataTask():
    
#______________________________________________________________________________
# Preallocating arrays for faster speed    
    # Data storage array for motor 1 speed
    pwmx = array.array('i',(0 for index in range(500)))
    # Data storage array for motor 2 speed
    pwmy = array.array('i',(0 for index in range(500)))
    # Data storage array for motor 1 angle
    motor1Angle = array.array('i',(0 for index in range(500)))
    # Data storage array for motor 2 angle
    motor2Angle = array.array('i',(0 for index in range(500)))

    ## Data storage array for x position
    x_pos = array.array('i',(0 for index in range(500)))
    ## Data storage array for y position
    y_pos = array.array('i',(0 for index in range(500)))
    ## Data storage array for x velocity
    x_d = array.array('i',(0 for index in range(500)))
    ## Data storage array for y velocity
    y_d = array.array('i',(0 for index in range(500)))
    ## Array for times, in seconds
    times = array.array('i',(0 for index in range(500)))
    ## Data collection rate in ms
    
    
    
    _state = 0
    x=0

    while True:
        if _state == 0:
            ## Z tells us if ball is off touch panel. True means ball is off panel. False means ball is on panel
            if shares.z.get() == 116:
                _state = 1
                _start = utime.ticks_ms()
                    
        if _state == 1:
            
                
            if shares.calib.get() == 116:
                motor1Angle[x] = (int(round(shares.eulerz.get(),1)*10)) # The data is converted to int's from floats to save memory.
                motor2Angle[x] = (int(round(shares.eulery.get(),1)*10))
            else:
                # Motor 1/2 position in radians
                motor1Angle[x] = (int(round(shares.enc1_pos.get(),1)*10))
                motor2Angle[x] = (int(round(shares.enc2_pos.get(),1)*10))
            # Motor 1/2 speed in rad/s
            pwmx[x] = (int(round(shares.pwmx.get(),1)*10))
            pwmy[x] = (int(round(shares.pwmy.get(),1)*10))
            # 
            times[x] = utime.ticks_diff(utime.ticks_ms(), _start) # in ms
            x_pos[x] = int(round(shares.x.get(),1)*10) 
            y_pos[x] = int(round(shares.y.get(),1)*10)
            x_d[x] = int(round(shares.x_d.get(),1)*10)
            y_d[x] = int(round(shares.y_d.get(),1)*10)
            
            x+=1
            if x >= 498 or shares.fault.get()==116: # Stops data collection before arrays are full to make sure data is not lost due to errors
                _state = 2
            
                
        if _state == 2:
            #print('Creating the CSV')
            _idx = 0
            try:
                with open('lab0xFF.csv',"w") as file:
                    file.write('{:},{:},{:},{:},{:},{:},{:},{:},{:} \r\n'.format('Times','X pos','Y pos', 'X Vel', 'Y vel', 'Pwmx', 'Pwmy', 'M1 Angle', 'M2 Angle'))
                    for _idx in range(len(times)): #Formatted so they are the correct values
                        file.write('{:},{:},{:},{:},{:},{:},{:},{:},{:} \r\n'.format(times[_idx], x_pos[_idx]/10, y_pos[_idx]/10,x_d[_idx]/10, y_d[_idx]/10, pwmx[_idx]/10, pwmy[_idx]/10, motor1Angle[_idx]/10, motor2Angle[_idx]/10))
            except KeyboardInterrupt:
                uos.sync()
            
            print('Finished CSV')
            #print(x_pos)
            _state = 3
            utime.sleep(0.5)
                    
        yield(0)
        
data = cotask.Task (dataTask, name = 'DataTask', priority = 0, 
                         period = 40, profile = True, trace = False)
cotask.task_list.append(data)     
    

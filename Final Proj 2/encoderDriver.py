"""@file encoderDriver.py
@brief Driver module for motor encoders
@details The module allows the user to input several hardware parameters, and provides class methods that can be called to get encoder position
@author: Daniel Freeman & Mathis Weyrich
"""
import pyb
import utime

class encoderDriver:
    '''@brief Class to operate encoders
       @details This class contains five methods to operate the encoders - init, update, get_position, set_position and get_delta. 
       When called regularly, these methods will allow the user to get the position of both encoders. 
    '''
    def __init__(self, e1p1, e1p2, e2p1, e2p2, tim1, tim2):
        '''@brief Method to set up hardware
        @details Sets up encoder hardware by taking in appropriate parameters
        @param e1p1 Pin 1 assigned to encoder 1
        @param e1p2 Pin 2 assigned to encoder 1
        @param e2p1 Pin 1 assigned to encoder 2
        @param e2p2 Pin 2 assigned to encoder 2
        @param tim1 Timer assigned to encoder 1
        @param tim2 Timer assigned to encoder 2
        '''
        ## Pin 1 on encoder 1 
        self.e1p1 = e1p1
        ## Pin 2 on encoder 1
        self.e1p2 = e1p2
        ## Pin 1 on encoder 2
        self.e2p1 = e2p1
        ## Pin 2 on encoder 2
        self.e2p2 = e2p2
        ## The period of the encoder, largest 16-bit number
        self.period = 65535
        ## Timer for encoder 1, using tim1
        self.timer = pyb.Timer(tim1, prescaler = 0, period = self.period)
        ## Encoder 1 channel 1
        self.t4ch1 = self.timer.channel(1, pyb.Timer.ENC_AB, pin = self.e1p1)
        ## Encoder 1 channel 2
        self.t4ch2 = self.timer.channel(2, pyb.Timer.ENC_AB, pin = self.e1p2)
        ## Timer for encoder 2, using tim2
        self.timer2 = pyb.Timer(tim2, prescaler = 0, period = self.period)
        ## Encoder 2 channel 1
        self.t8ch1 = self.timer2.channel(1, pyb.Timer.ENC_AB, pin = self.e2p1)
        ## Encoder 2 channel 2
        self.t8ch2 = self.timer2.channel(2, pyb.Timer.ENC_AB, pin = self.e2p2)
        ## The initial position of encoder 1
        self.position1 = 0
        ## The initial position of encoder 2
        self.position2 = 0
        ## The start time of the program
        self.thisTime = utime.ticks_ms()
        ## The initial delta of encoder 1
        self.delta1 = 0
        ## The initial delta of encoder 2
        self.delta2 = 0
        ## The inital encoder 1 count
        self.count1 = self.timer.counter()
        ## The initial encoder 2 count
        self.count2 = self.timer2.counter()
        #print ('Creating an encoder driver')
        # ONE REVOLUTION IS 4000 TICKS
    
    def update(self):
        '''@brief Updates recorded position of encoder
        @details When called regularly, updates recorded position of encoder
        '''
        #print('Update method running')
        self.delta1 = self.timer.counter() - self.count1
        if self.delta1 > -0.5*self.period and self.delta1 < 0.5*self.period:
            pass
        elif self.delta1 > 0.5*self.period:
            self.delta1 = self.delta1 - self.period
        elif self.delta1 < 0.5*self.period:
            self.delta1 = self.delta1 + self.period
        self.position1 += self.delta1 
        self.count1 = self.timer.counter()
        
        
        self.delta2 = self.timer2.counter() - self.count2
        if self.delta2 > -0.5*self.period and self.delta2 < 0.5*self.period:
            pass
        elif self.delta2 > 0.5*self.period:
            self.delta2 = self.delta2 - self.period
        elif self.delta2 < 0.5*self.period:
            self.delta2 = self.delta2 + self.period
        self.position2 += self.delta2 
        self.count2 = self.timer2.counter()
        
    def get_position(self):
        '''@brief Returns the most recently updated position of the encoder
        @returns self.position The updated encoder position
        '''
        
        return self.position1, self.position2
    
    def set_position(self,pos1, pos2):
        '''@brief Resets the position to a specified value
        '''
        self.position1 = pos1
        self.position2 = pos2
    
    def get_delta(self):
        '''@brief Returns the difference in recorded position between the two most recent calls to update()
            @returns self.delta The difference in position
        '''
        return self.delta1, self.delta2
        

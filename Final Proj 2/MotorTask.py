"""
@file       MotorTask.py
@brief      Runs the motors based on the PWM given by the controller task

@date       Tue May 25 09:23:07 2021

@author:    Mathis Weyrich
"""

# Motor Driving Task
import pyb, shares, task_share, cotask, print_task
from MotorDriver import DRV8847


def MotorTask():
    '''
    @brief          Runs the motors based on the PWM given by the controller task
    @details        Initializes two motor objects using the DRV8847 driver. Also 
                    handles faults and enable. Runs initialized motors at the given PWM
                    values.
    

    '''
    _drv = DRV8847(pyb.Pin.cpu.A15, pyb.Pin.cpu.B2) #passes in the pin that controls sleep and fault
    _M1 = _drv.channel(pyb.Pin.cpu.B4, pyb.Pin.cpu.B5,3, 1,2) # Activates Motor 1 on timer 3 channels 1/2
    _M2 = _drv.channel(pyb.Pin.cpu.B0, pyb.Pin.cpu.B1,3, 3,4) # Activate Motor 2 on timer 3 with channels 3/4
    _drv.enable()
    
    while True:
        if _drv == 'Fault': # Activates if a fault is detected
            shares.fault.put(116) # Activated the fault varaible in shares
            print_task.put_bytes(b'Fault Detected')
            
        if shares.enable == 116: # Activates when the UI passes the enable to true
            _drv.clear_fault()
            _drv.enable()
            shares.enable.put(102) # resets the variable
            
        _M1.set_level(shares.pwmx.get()) # Runs the motor at the desired PWM
        _M2.set_level(shares.pwmy.get())
        
                
                
        yield(0)    # To exit out of the generator
        
motask = cotask.Task (MotorTask, name = 'Motask', priority = 1, 
                         period = 30, profile = True, trace = False)
cotask.task_list.append (motask)   
        
        


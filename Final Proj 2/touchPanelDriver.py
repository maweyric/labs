"""@file touchPanelDriver.py
@brief Driver for touch panel
@details This driver allows interaction with the touch panel hardware. The driver contains a constructer, which sets up the pins for use. The
driver also contains three functions, responsible for reading the ADC from the necessary pins, converting it to a distance, and returning 
that value
@author: Daniel Freeman & Mathis Weyrich
"""
import pyb, utime

class touchPanelDriver:
    '''@brief Class for touch panel operation
    @details Class contains a constructor to initiate pins and establish panel dimensions. Three functions also read the ADC count from 
    each pin, and converts it to a distance in mm
    '''
    def __init__(self,length, width):
        '''@brief Constructor for touch panel
        @details Sets up hardware pins and panel dimensions for use
        @param length The length of the touch panel, in mm
        @param width The width of the touch panel, in mm
        '''
        ## Negative X pin
        self.xm = pyb.Pin(pyb.Pin.cpu.A1)
        ## Positive X pin
        self.xp = pyb.Pin(pyb.Pin.cpu.A7)
        ## Negative Y pin
        self.ym = pyb.Pin(pyb.Pin.cpu.A0)
        ## Positive Y pin
        self.yp = pyb.Pin(pyb.Pin.cpu.A6)
        ## Length of platform in mm
        self.length = length
        ## Width of platform in mm
        self.width = width

    def y_pos(self):
        '''@brief Gets x-position in mm
        @details Sets both x pins to output push-pull pins. Reads ADC from the negative y pin. Converts this ADC count to an x position, where 0mm is the centerline of the board
        @returns x x position in mm
        '''
        
        # This one gives sensible results when initialized first - Max ~55, min ~-55
        self.yp.init(mode=pyb.Pin.IN)
        self.ym.init(mode=pyb.Pin.IN)
        self.xm.init(mode=pyb.Pin.OUT_PP)
        self.xm.low()
        self.xp.init(mode=pyb.Pin.OUT_PP)
        self.xp.high()
        utime.sleep_us(4)
        ## ADC reading from negative Y pin
        adc = pyb.ADC(self.ym)
        ## ADC reading in x-direction
        x_adc = adc.read()
        ## x position in mm, relative to origin
        x = 0.035*x_adc - 73.922
        return x
    
    def x_pos(self):
        '''@brief Gets y-position in mm
        @details Sets both y pins to output push-pull pins. Reads ADC from the negative x pin. Converts this ADC count to an y position, where 0mm is the centerline of the board
        @returns y y position in mm
        '''
        #working now
        self.xp.init(mode=pyb.Pin.IN)
        self.xm.init(mode=pyb.Pin.IN)
        self.yp.init(mode=pyb.Pin.OUT_PP)
        self.yp.high()
        self.ym.init(mode=pyb.Pin.OUT_PP)
        self.ym.low()
        utime.sleep_us(4)
        ## ADC reading from negative X pin
        adc = pyb.ADC(self.xm)
        ## ADC reading in y direction
        y_adc = adc.read()
        ## y position in mm, relative to origin
        y = 0.0544*y_adc - 113.15
        return y

        
    def z_pos(self):
        '''@brief Tells user if ball is on board
        @details Sets x minus and y plus pins to output push-pull pins. Reads ADC from the negative y pin. Converts this ADC count to tell if ball is either on (count is around 4095)
        or off (count is around 300) the touch panel. 
        @returns True Ball is on board
        @returns False Ball is off board
        '''
        self.xm.init(mode = pyb.Pin.OUT_PP)
        self.xm.low()
        self.yp.init(mode = pyb.Pin.OUT_PP)
        self.yp.high()
        ## ADC reading from negative Y pin
        adc = pyb.ADC(self.ym)
        ## ADC reading in z direction
        z_adc = adc.read()
        if z_adc > 4000:
            return False
        else:
            return True
                 
    
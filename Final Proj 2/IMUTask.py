"""
@file       IMUTask.py
@brief      Initializes and I2C connection with the IMU chip, and returns Euler angles and Angular speed
@details    
@date       Tue May 25 11:31:28 2021
@author    Mathis Weyrich
"""

# IMU task

import shares, task_share, cotask, print_task
from pyb import I2C
from BNO055 import BNO


def IMUTask():
    '''
    @brief      Initializes an I2C connection with the IMU chip, and returns 
                Euler angles and Angular speed.
    @details    Euler and Angular speed are added to shares on an individual basis (not a list)

    '''
    _state = 0
    _i2c = I2C(1, I2C.MASTER) # Setting up the I2C connection
    _i2c.init() 
    imu = BNO(_i2c) # passes the I2C into the IMU driver
    
    while True:
        if _state == 0:
            
            if shares.op_mode.get() == 73: # If Ui sends the data to make it in IMU mode
                imu.Op_mode('IMU')
                shares.op_mode.put(67)
                _state = 1
                
        if _state == 1:
            imu.calib() # Need to fix the calibration status of the BNO
            if imu.calib() == 'calibrated': #Place holder
                _state = 2
                print_task.put_bytes(b'Finished Calibrating\r\n')
                shares.calib.put(116)
                
        if _state == 2:
            _euler = imu.Angles()
            shares.eulerx.put(_euler[0])
            shares.eulery.put(_euler[1])
            shares.eulerz.put(_euler[2])
            _omega = imu.Omega()
            shares.omegax.put(_omega[0])
            shares.omegay.put(_omega[1])
            shares.omegaz.put(_omega[2])
            
            
                
        yield(0)   
    
imutask = cotask.Task (IMUTask, name = 'IMUTask', priority = 1, 
                         period = 35, profile = True, trace = False)
cotask.task_list.append (imutask)         
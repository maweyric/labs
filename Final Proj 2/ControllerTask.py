"""
@file       ControllerTask.py
@brief      Uses state values from shares.py to output the required PWM
@details    
@date       Tue May 25 09:37:46 2021

@author: Mathis Weyrich
"""

# Controller

# Kp = (100*R/(4*Kt*Vdc))*K, where K is the vector of gains calculated in matlab
    # R = Motor resistance
    # Vdc = Voltage being supplied
    # Kt = Tload/Vmotor
# Duty percent= Kp*X, where x = [x, thy, x_d, thy_d]
# Need freq_pwm > R/2*pi*L, between 20 - 100 kHz
    # L = motor inductance

import shares, task_share, cotask, print_task, utime
def ClosedLoop(): # Need this to be a function for scheduler purposes
    '''
    @brief      Calculates the necessary PWM for both motors
    @details    Uses motor constants as well as data from the encoder and touch panel to identify where
                the ball is and what the system's current response is. From this data, a new PWM for 
                motor is calculated and sent into shares.
                The gains found mathematically have been altered and tuned to the system. In addition,
                a filter to bump up the PWM past the frictional point of the motors to better simulate
                the low PWM's has been added for theoretical PWM's between 7-40. The controller will
                use the IMU for the angles and angular velocity when the IMU is calibrated. 
    '''
    
    _R = shares.R.get()
    _Vdc = shares.Vdc.get()
    _Kt = shares.Kt.get()
    
    _x = shares.x.get()
    _y = shares.y.get()
    
    _K = [-0.3553/0.12, -0.3045/0.09, -0.1283/0.02, -0.0232/0.04] # Gains that were last used before alpha beta filter
    #_K = [-0.3553/1, -0.3045/1, -0.1283/1, -0.0232/1]
    _Kp = [0,0,0,0]
    for x in range(len(_K)):
        _Kp[x] = (100*_R/(4*_Kt*_Vdc))*_K[x]

    
    while True:
        #print_task.put_bytes(b'C\r\n')
        # make an off state
        if shares.z.get() != 116:
            pass
        else:
            
            # Setting all the variables for the x state vector
            _x = shares.x.get()
            _x_d = shares.x_d.get()
            
             # Setting all the variables for the y state vector
            _y = shares.y.get()
            _y_d = shares.y_d.get()
            
            
            if shares.calib.get() == 116: # Will use the IMU data is it is calibrated
                _thy = shares.eulery.get()
                _thy_d = shares.omegay.get()
                _thx = shares.eulerz.get()
                
                _thx_d = shares.omegax.get()
                
            else: # Otherwise uses the encoder
                _thy = shares.thy.get()
                _thy_d = shares.thy_d.get()
                _thx = shares.thx.get()
                _thx_d = shares.thx_d.get()
            
            _xvec = [_x, _thy, _x_d, _thy_d] # The state vector that controls the motion of the ball in the x direction
            _yvec = [_y, _thx, _y_d, _thx_d] # The state vector that controls the motion of the ball in the y direction
            
            #===============================================================================
            # # Alpha Beta Filter
            # alpha = 2
            # beta = 0.5
            # T = 0.6 #Time between data points
            
            # _xpred2 = shares.xpred.get() + alpha*(_x-shares.xpred.get()) + T*shares.x_dpred.get()
            # shares.xpred.put(_xpred2)
            # _x_dpred2 = shares.x_dpred.get() + beta/T*(_x-shares.xpred.get())
            # shares.x_dpred.put(_x_dpred2)
            
            # _ypred2 = shares.ypred.get() + alpha*(_y-shares.ypred.get()) + T*shares.y_dpred.get()
            # shares.ypred.put(_ypred2)
            # _y_dpred2 = shares.y_dpred.get() + beta/T*(_y-shares.ypred.get())
            # shares.y_dpred.put(_y_dpred2)
            
            #_xvec = [_xpred2, _thy, _x_dpred2, _thy_d] # The state vector that controls the motion of the ball in the x direction
            #_yvec = [_ypred2, _thx, _y_dpred2, _thx_d] # The state vector that controls the motion of the ball in the y direction
            
            # # Notice: As it goes on, the PWM values start getting exponentially larger - probably due to a constant adding of the previous values
            
            
            
            
            
            _pwmy = (_Kp[0]*_xvec[0]+_Kp[1]*_xvec[1]+_Kp[2]*_xvec[2]+_Kp[3]*_xvec[3]) # Switched these, as it appears somewhere the x/y were switched
            
            
            _pwmx = -(_Kp[0]*_yvec[0]+_Kp[1]*_yvec[1]+_Kp[2]*_yvec[2]+_Kp[3]*_yvec[3]) # Also needed to make these negative to move in the correct direction
#_______________________________________________________________________________________________________
# Filter to increase the base PWM of the motors to get past the motor stiction
           
            if _pwmx >7 and _pwmx<40: # Could potentially use a plus equals to fine tune pwm
                _pwmx = 0.455*_pwmx +21.82              # M1 is a bit stickier than M2
            # elif _pwmx >15 and _pwmx<30:
            #     _pwmx = 45
            elif _pwmx <-7 and _pwmx>-40:
                _pwmx = 0.455*_pwmx -26.82  
            # elif _pwmx <-15 and _pwmx>-30:
            #     _pwmx = -55
                
            if _pwmy >7 and _pwmy<40: # Could potentially use a plus equals to fine tune pwm
                _pwmy = 0.455*_pwmy +21.82  
            # elif _pwmy >15 and _pwmy<30:
            #     _pwmy = 40
            elif _pwmy <-7 and _pwmy>-40:
                _pwmy = 0.455*_pwmy -26.82 
            # elif _pwmy <-15 and _pwmy>-30:
            #     _pwmy = -50
            
#______________________________________________________________________
# Analysis and questions for controller            
            # ___________________________  +
            # |             |            | ^
            # |             |            | | Motor 2 attached at (.) - Negative Pwmy makes M2 go up
            # |_____________|____________. y
            # |             |            | |
            # |             |            | v
            # |_____________.____________| -
            #         - <-- x --> +
            # Motor1 Attached at (.) - Negative Pwmx makes M1 go up
            
            # Motor 1 has a better response in the positive y direction (as the gravity of the ball helps)
            # and a worse response in the negative y direction (as the ball is actively increasing the force)
            # Is there a way to make the controller output a larger output on one side versus the other?
            
            # Further Problem - sometimes the motor just "drifts" Keeps on going even after there should be 
            # no more input - does that on test
            
            #When the ball is first placed, there is no reaction for a little bit - then it over reacts
            
            # How to identify time between runs/data points?
            
            # Touch pad sometimes stops working? - View Example CSV
            
            # Aliright on the initial movement - bad at the counter movement (following up)
            
            
#__________________________________________________________________________
# Code segment to print PWM and position on touchpad
            # if _pwmx != shares.pwmx.get():
            #     px = str(round(_pwmy,1))
            #     xv = str(round(_x))
            #     print_task.put(px+'p' + xv+'>X\r\n')
                
            # if _pwmy != shares.pwmy.get():
            #     py = str(round(_pwmx,1))
            #     yv = str(round(_y))
            #     print_task.put(py+'p' + yv +'>Y\r\n')
                
            
            if _pwmx >= -99 and _pwmx <=99: # Ensuring that the Pwm achieved is not above saturation limits
                shares.pwmx.put(_pwmx)
            elif _pwmx > 99:
                shares.pwmx.put(99)
            elif _pwmx < -99:
                shares.pwmx.put(-99)
                
            if _pwmy>= -99 and _pwmy <=99:
                shares.pwmy.put(_pwmy)
            elif _pwmx > 99:
                shares.pwmy.put(99)
            elif _pwmx < -99:
                shares.pwmy.put(-99)
                
            
            
        
                
        yield(0)    # need this to exit the scheduler
                
controllertask = cotask.Task (ClosedLoop, name = 'ContTask', priority = 1, 
                         period = 30, profile = True, trace = False)
cotask.task_list.append(controllertask)   
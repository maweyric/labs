#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file Motor.py

@brief    This file cosists of a Motor Diver class file which has the main operation to 
          control a motor when conected to arbitrary pins. 

@details  This file is meant to stricly run in micropython since it uses pyb.Timer class.
          This code purpose is to enable/disable the motor and set the duty cycle of the
          motor. It also has a callback method that when trigger, it will protect the 
          motors.


@author Matthew Pfeiffer
@author: Adan Martinez
@date March 2, 2021
"""
import pyb

class MotorDriver:
    ''' 
    This class implements a motor driver for the ME405 board. 
    '''
    global fault
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, channelA, channelB, timer, nFault_pin): 
        ''' 
        Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        
        @param nSLEEP_pin   A pyb.Pin object to use as the enable/disable pin.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param channelA     A channel variable to use with IN1_pin.
        @param channelB     A channel variable to use with IN2_pin.
        @param timer        A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
        @param nFault       A pin use for the nFault pin.
        '''
        print ('Creating a motor driver') 
        
        ## Creates a variable for the nSLEEP_pin object.
        self.nSLEEP_pin = nSLEEP_pin
                
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
 
        ## Creates a variable  for the channel value.
        self.channelA = channelA
        self.channelB = channelB
        ## Creates a variable for timer object.
        self.timer = timer
        ## Disables the motors for safety.
        self.nSLEEP_pin.low()
                
        self.timchA = self.timer.channel(self.channelA, pyb.Timer.PWM, pin=self.IN1_pin)
        self.timchB = self.timer.channel(self.channelB, pyb.Timer.PWM, pin=self.IN2_pin)

        ## Setting Up Pin for Interruptor    
        #self.extint = pyb.ExtInt(pyb.Pin.cpu.B2,pyb.ExtInt.IRQ_RISING,pyb.Pin.PULL_UP, myCallback)
        self.nFAULT_pin = nFault_pin
        self.fault = False
            
    def enable(self):
        '''
        @brief       This method powers on the motor.
        '''
        if self.fault == True:
            print("POWER OFF")
            self.nSLEEP_pin.low()
            while True:
                self.user_input = input('To reset FAULT PIN please enter K: ')
                if self.user_input == 'K':
                    self.fault = False
                    self.NSLEEP_pin.high()
                    break
        else:
            print ('Enabling Motor')
            ## Enables the motor.
            self.nSLEEP_pin.high()
        
    def disable(self):
        '''
        @brief   A method that powers off the motors. 
        '''
        if self.fault == True:
            print("POWER OFF")
            self.nSLEEP_pin.low()
            while True:
                self.user_input = input('To reset FAULT PIN please enter K: ')
                if self.user_input == 'K':
                    self.fault = False
                    self.NSLEEP_pin.high()
                    break
        else:
            print ('Disabling Motor')
            ## Disables the motor.
            self.nSLEEP_pin.low()
        
    def set_duty(self, duty):
        ''' 
        @brief       This method sets the duty cycle to be sent to the motor to the given level. Positive values 
                     cause effort in one direction, negative values in the opposite direction.
        @param duty  A signed integer holding the duty
                     cycle of the PWM signal sent to the motor.
        '''
        ## creates a variable for the Duty cycle.
        self.duty = duty
        print('Running at ' + str(self.duty) + ' percent duty cycle.')
        ## Sets the pulse width percent equal to 0 for both channels. 
        self.timchA.pulse_width_percent(0)
        self.timchB.pulse_width_percent(0)
        
        if self.fault == True:
            print("Motor OFF")
            self.nSLEEP_pin.low()
            while True:
                self.user_input = input('To reset FAULT PIN please enter K: ')
                if self.user_input == 'K':
                    self.fault = False
                    self.NSLEEP_pin.high()
                    break
        elif self.fault == False:
            ## Takes cares of negative duty cycle values.
            if self.duty < 0:
                print('Moving CW')
                self.timchB.pulse_width_percent(abs(self.duty))
            ## Takes care of positive duty cycles.
            elif self.duty > 0:
                print('Moving CCW')
                self.timchA.pulse_width_percent(self.duty)
            else:
                print('Error...')
                pass

    def myCallback(self):
        '''
        @brief         A function that serves as the interrupt.
        @detail        The interrupt callback function then reads the timer to see 
                       how many microseconds have elapsed since the LED was turned on.
        '''
        self.fault = True
        print('Fault Pin Trigger!')
    # Setting Interruptor, Prof Wade said to use Pin B2. The data sheet specifies pinA6
    extint = pyb.ExtInt(pyb.Pin.cpu.B2, pyb.ExtInt.IRQ_RISING, pyb.Pin.PULL_UP, myCallback)

if __name__ == '__main__':
    ## Any code within the if __name__ == '__main__' block will only run when 
    # the script is executed as a standalone program. If the script is 
    # imported as a module the code block will not run.
    
    ## Create the pin objects used for interfacing with the motor driver
    pin_nSLEEP  = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    pin_IN1     = pyb.Pin(pyb.Pin.cpu.B4)  #pyb.Pin.OUT_PP) 
    pin_IN2     = pyb.Pin(pyb.Pin.cpu.B5)  #pyb.Pin.OUT_PP)
    pin_IN3     = pyb.Pin(pyb.Pin.cpu.B0)  #pyb.Pin.OUT_PP)
    pin_IN4     = pyb.Pin(pyb.Pin.cpu.B1)  #pyb.Pin.OUT_PP)
    
    ## Sets value for each channel. 
    ch1 = 1
    ch2 = 2
    ch3 = 3
    ch4 = 4
    
    # Create the timer object used for PWM generation
    timer = pyb.Timer(3, freq=20000)
    
    # Create a motor object passing in the pins and timer
    moe1 = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, ch1, ch2, timer)
    moe2 = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, ch3, ch4, timer)
    
    # Enable the motor driver
    moe1.enable()
    moe2.enable()
    
    # Set the duty cycle at 33 percent. A positive value rotates the shaft CCW
    # and a negative value rotates the shaft CW.
    moe1.set_duty(50)
    moe2.set_duty(50)
    
    #moe = MotorDriver(pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP),pyb.Pin(pyb.Pin.cpu.B4),pyb.Pin(pyb.Pin.cpu.B5),1,2,pyb.Timer(3, freq=20000),1)
    
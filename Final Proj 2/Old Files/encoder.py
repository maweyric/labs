#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file encoder.py

This file cosists of an Encoder class file which has the main operation to use 
the timer counter to read from an encoder conected to arbitrary pins. 

This file is meant to stricly run in micropython since it uses pyb. The 
code implemented main goal is to read from timer counter to control and obtain an 
encoders position when connected through a Nucleo Board.

@author: Adan Martinez
@author Matthew Pfeiffer
@date March 2, 2021
"""
import pyb
#import shares
import utime


class EncoderDriver:
    '''
    @brief      A class that is meant to control and retrieve position from encoders.
    @details    This class implements the four methods. The update,  get position, 
                get delta, and set position methods.
    '''
    
    def __init__(self, tim_num, pin1, pin2, interval):
        '''
        @brief            Creates an Encoder object.
        @param interval   An object from class interval representing time between
                          each running states.
        '''
        ## The following lines allow the file to access timer counter. 
        self.tim_num = tim_num
        self.pin1 = pin1
        self.pin2 = pin2
        
        self.tim = pyb.Timer(self.tim_num) #This sets the timer equal to 4.
        self.tim.init(prescaler=0, period=65535) #Sets the timer prescaler to 0 and maximun value of a 16 bit counter.
        # pyb.Pin.cpu.B6
        # pyb.Pin.cpu.B7
        self.pinB6 = self.tim.channel(1, pin=self.pin1, mode=pyb.Timer.ENC_AB) # Sets connection to pin A6
        self.pinB7 = self.tim.channel(2, pin=self.pin2, mode=pyb.Timer.ENC_AB) # Sets connection to pin A7

        self.counter = 0 # Set tim.counter() to a variable counter.
        self.counter = self.tim.counter()
        ## This represents the maximun value for a 16-bit counter which is 2^16
        self.period = int(65536)
        ## Setting the initial delta.
        self.fixed_delta = 0
        ## Sets the inital position equal to zero.
        self.position = 0
        ## Setting angle
        self.ticks_deg = (1000*4/360)  #11.11 ticks/deg
        
        ## Initial angle
        self.angle = 0
        
        ##  The amount of time in seconds between runs of the task. Its converted from micro-sec to sec.
        #   By default the interval is 1s to change go to shares.py
        self.interval = int(interval*(1e6))        
        ## The timestamp for the first iteration 
        self.start_time = utime.ticks_us()
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        print('Setting Up')
        
    def update(self):
        '''
        @brief      This method updates the recorded position of the encoder. 
                    Also, runs one iteration of the task. 
        '''
        ## Updating current timestamp
        self.curr_time = utime.ticks_us()
        
        ## Takes our initial tim.counter
        self.init_counter = self.counter
        
        self.counter = self.tim.counter()
        
        ## Overflow/Underflow delta
        self.OU_delta = (self.counter - self.init_counter)
        
        ## Taking care of good deltas. Checks that the magnitude of the delta 
        #  is smaller then half of our 16 bit max value count.
        if(self.OU_delta < (self.period/2) and self.OU_delta > (-1*self.period/2)):
            ## Get delta
            self.delta = self.counter - self.init_counter
            self.fixed_delta = self.delta

        ## This takes care of bad deltas that are greater than half of our 16 
        #  bit max value.
        elif(self.OU_delta > self.period/2):
            self.fixed_delta = self.OU_delta - self.period
            ## Get delta
            # self.delta = (self.counter - self.init_counter)
            
            # ## Handles negative "bad" deltas.
            # if(self.delta < 0):
            #     ## To correct negative "bad" delta add the period(max bit 
            #     #  count value)
            #     self.fixed_delta = self.delta + self.period
                
            # ## Handles positive "bad" deltas.
            # elif(self.delta > 0):
            #     ## To correct positive "bad" deltal subtract the 
            #     #  period(max bit count value)
            #     self.fixed_delta = self.delta - self.period
        elif self.OU_delta < (self.period/2):
            self.fixed_delta = self.OU_delta + self.period
        else:
            # Invalid state code (error handling)
            self.fixed_delta = 0
            
        # Specifying the next time the task will run
        self.next_time += utime.ticks_add(self.next_time, self.interval)
        
        
        ## Constanly add to the position so it does not reset.
        self.position = self.position + self.fixed_delta
        
        ## Using updated position to calculate total angle of rotation
        self.angle = self.position*(1/self.ticks_deg)
        
        ## Calculating speed and converting to rad/sec
        #self.speed = (self.angle/shares.interval)*0.0174532925 # rad/sec
        
    def get_position(self):
        '''
        @brief   Uses pyb to read motor position from encoders and returns
                 the value in counts.
        '''
        self.update()
        return self.position
    
    def set_position(self):
        '''
        @brief   A method that resets the motor position to a 0.
        '''
        self.position = 0
        print('Position is set to ' + str(self.position))

    def get_angle(self):
        '''
        @brief   Returns the postition in degrees.     
        '''
        self.update()
        return self.angle
    
    def get_delta(self):
        '''
        @brief   Returns the difference in recorded position between the two 
                 most recent calls to update(). Returns the value in degrees..        
        '''
        self.update()
        self.fdelta = self.fixed_delta*(1/self.ticks_deg)
        return self.fdelta
    def get_speed(self):
        '''
        @brief   Returns the speed in [deg/sec].      
        '''
        self.update()
        self.sdelta = self.fixed_delta*(1/self.ticks_deg)
        self.vel = self.sdelta/self.interval
        return self.vel
    
if __name__ == '__main__':
    
    ## Setting time interval.
    interval = 5
    ## Creating Encoder Objects
    ENC1 = EncoderDriver( 4, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7,interval)
    ENC2 = EncoderDriver( 8, pyb.Pin.cpu.C6, pyb.Pin.cpu.C7,interval)
    while True:
        ENC1.get_position()
        


    
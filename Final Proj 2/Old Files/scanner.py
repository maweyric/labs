#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file scanner.py

@brief     A Scan class driver for reading from a resistive touch panel.
@details   A Scan class driver for reading from a resistive touch panel with a
           STM32 microcontroller. This file can run coperatively with when called.
           The Scan class takes eigth inputs. Which consist of the four pin objects,
           panel dimensions, and the coordinates at the center.
           
Created on Mon Oct 19 20:25:57 2020

@author: Adan Martinez
"""
import pyb
from pyb import Pin
from pyb import ADC
import utime
#import array


class Scan:
    '''
    @brief      A class that is scans and returns a value when something touches
                the touch panel.
    @details    This class consists of scaning the touch panel for any voltage
                in the panel. The constructor consists of taking eigth inputs.
                These are the pin objects, the length and width of the touch 
                panel, and the two coordinate of the center of the panel. 
                This class includes four methods; readX(), readY(), 
                readZ(), and readALL().
    '''

    def __init__(self, Pin_xm, Pin_xp, Pin_ym, Pin_yp, length, width, x0, y0):
        '''
        @brief          Creates a Scan object.
        @param Pin_xp       Represents the pin connected to the x- of the touch panel.
        @param Pin_xm       Represents the pin connected to the x+ of the touch panel.
        @param Pin_ym       Represents the pin connected to the y- of the touch panel.
        @param Pin_yp       Represents the pin connected to the y+ of the touch panel.
        @param length   Represents length of the touch panel in mm.
        @param width    Represents width of the touch panel in mm.
        @param x0       Represents center of the touch panel in the x direction.
        @param y0       Represents center of the touch panel in the y direction.
        '''
        ## Creating obbject for Pins and for lenght and width of the touch panel.
        self.xm = Pin_xm
        self.PIN_xm = Pin(self.xm) #A7
        self.PIN_xp = Pin(Pin_xp) #A6
        self.PIN_ym = Pin(Pin_ym) #A1
        self.PIN_yp = Pin(Pin_yp) #A0
        ## Testing
        self.ADC_ym = ADC(Pin.cpu.A1)
        self.ADC_xm = ADC(Pin.cpu.A7)
        
        self.L  = length/2 # Panel's Length in mm
        self.W  = width/2  # Panel's Width in mm
        ## The coordinates of the center of the touch panel.
        self.x_center = x0
        self.y_center = y0
        self.x_conv = (self.L/4095)*2
        self.y_conv = (self.W/4095)*2
    def readX(self):
        '''
        @brief      This method reads and convert the binary reading value from
                    the ADC that reads from the touch panel. It only handles the
                    readings in the x direction. It takes the center of the touch
                    panel as a zero value. And, depending in the orientation, it
                    returns a positive or negative value if in the x- or x+
                    direction.
        '''
        ## Creating and Initializing Pins. 
        # self.PIN_1xm = Pin(Pin.cpu.A7)
        # self.PIN_1xp = Pin(Pin.cpu.A6)
        ## Creating the pin to read from.
        # self.ADC_ym = ADC(Pin.cpu.A1)
        
        self.PIN_ym = Pin(Pin.cpu.A1, mode=Pin.IN)
        self.PIN_xm.init(mode=Pin.OUT_PP, value=0)
        self.PIN_xp.init(mode=Pin.OUT_PP, value=1)
        self.PIN_ym.init(mode=Pin.ANALOG)
        self.ADC_ym = ADC(self.PIN_ym)
        pyb.udelay(4)
        # Reading from ADC pin and normalized it. Dividing bby 4095 because the
        # panels reads at a 12 bit resolution.
        self.xpos = self.ADC_ym.read()*self.x_conv
        if self.xpos < self.L:
            ## If the x position is less than L/2 that means is in the
            #  negative side of the panel. Therefore we make it a negative
            #  value.
            self.xpos = (self.xpos*(-1)+self.L)*-1

        elif self.xpos > self.L:
            ## If x postion is larger than L/2, this means its in the
            #  positive side of the panel and its substracted 4inches.
            self.xpos = self.xpos-self.L
        elif self.xpos == self.L:
            ## If x position is equal to L/2, its at the center of the horizontal
            #  direction, therefore its set to zero.
            self.xpos = 0
        return self.xpos


    
    def readZ(self):
        '''
        @brief   This method sets up the pin object for reading from the touch
                 panel. This method reads the z value. It returns a Boolean value 
                 representing whether or not something is in contact with the 
                 touch panel. A value of 0 means no contact and a 1 means contact.
        '''
        #self.PIN_xm = Pin(Pin.cpu.A7)
        # self.PIN_yp = Pin(Pin.cpu.A0)
        # 
        # self.ADC_ym = Pin(Pin.cpu.A1)
        
        self.PIN_ym = Pin(Pin.cpu.A1, mode=Pin.IN)
        self.PIN_xm.init(mode=Pin.OUT_PP, value=0)
        self.PIN_yp.init(mode=Pin.OUT_PP, value=1)
        self.PIN_ym.init(mode=Pin.ANALOG)
        
        self.ADC_ym = ADC(self.PIN_ym)
        pyb.udelay(4)
        self.zpos = self.ADC_ym.read()
        
        if self.zpos >= 4079:
            ## Return a 0 value when the reading is greater than or equal to
            #  4079, meaning the pad is not sensing anything. 
            self.zpos = 0
        elif self.zpos < 4079:
            ## Returns a 1 value when the reading is less than 4079 meaning the
            #  there is sensing some mass. 
            self.zpos = 1
        return self.zpos
    
    def readY(self):
        '''
        @brief      This method returns values within the range of width and 
                    length specified in the constructor such that the readings 
                    are zero if the point of contact is in the middle of the 
                    touch panel.
        '''
        # self.PIN_ym = Pin(Pin.cpu.A1)
        # self.PIN_yp = Pin(Pin.cpu.A0)
        
        # self.ADC_xm = ADC(Pin.cpu.A7)
        self.PIN_xm = Pin(Pin.cpu.A7, mode=Pin.IN)

        self.PIN_ym.init(mode=Pin.OUT_PP, value=0)
        self.PIN_yp.init(mode=Pin.OUT_PP, value=1)
        self.PIN_xm.init(mode=Pin.ANALOG)
        self.ADC_xm = ADC(self.PIN_xm)
        pyb.udelay(4)
        self.ypos = self.ADC_xm.read()*self.y_conv

        if self.ypos > self.W:
            ## If y postion is larger than half of the width, this means its in the
            #  positive side of the panel and its substracted 4inches.
            self.ypos = self.ypos-self.W
        elif self.ypos < self.W:
            ## If the y position is less than half of the total width that means 
            #  is in the -y side of the panel. Therefore we make it a negative
            #  value.
            self.ypos = (self.ypos*(-1)+self.W)*-1
        elif self.ypos == self.W:
            ## If y position is equal to the half of the width, this means that
            #  the obkect is at the center of the vertical
            #  direction, therefore its set to zero.
            self.ypos = 0
        return self.ypos
    
    def readALL(self):
        '''
        @brief   Reads from the readX(), readY(), and readZ() methods and saves
                 the data into a tuple.  It also add a time bechmark of how long
                 it take to read all three values.
        '''
        self.start_time = 0
        self.start_time = utime.ticks_us()
        self.xloc = self.readX()
        self.zloc = self.readZ()
        self.yloc = self.readY()
        self.finish_time = utime.ticks_us()
        self.benchmark = utime.ticks_diff(self.finish_time,self.start_time)
        self.data = (self.xloc,self.zloc,self.yloc,self.benchmark)
        return self.data

if __name__ == '__main__':
    ## Any code within the if __name__ == '__main__' block will only run when 
    # the script is executed as a standalone program. If the script is 
    # imported as a module the code block will not run.
    PIN_xm = Pin(Pin.cpu.A7) #A7
    PIN_xp = Pin(Pin.cpu.A6) #A6
    PIN_ym = Pin(Pin.cpu.A1) #A1
    PIN_yp = Pin(Pin.cpu.A0) #A0
    pass
    
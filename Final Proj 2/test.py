# -*- coding: utf-8 -*-
"""
Created on Tue Jun  8 14:38:06 2021

@author: HP
"""

import MotorDriver, utime, pyb, shares, BNO055, encoderDriver
from pyb import I2C

drv = MotorDriver.DRV8847(pyb.Pin.cpu.A15, pyb.Pin.cpu.B2)
M1 = drv.channel(pyb.Pin.cpu.B4, pyb.Pin.cpu.B5,3, 1,2)
M2 = drv.channel(pyb.Pin.cpu.B0, pyb.Pin.cpu.B1,3, 3,4)
drv.enable()

## Encoder 1 pin 1
e1p1 = pyb.Pin(pyb.Pin.cpu.B6)
## Encoder 1 pin 2
e1p2 = pyb.Pin(pyb.Pin.cpu.B7) 
## Encoder 2 pin 1
e2p1 = pyb.Pin(pyb.Pin.cpu.C6)
## Encoder 2 pin 2
e2p2 = pyb.Pin(pyb.Pin.cpu.C7) 
## Timer 1
tim1 = 4 
## Timer 2
tim2 = 8
## Object of encoder class
encoder = encoderDriver.encoderDriver(e1p1, e1p2, e2p1, e2p2, tim1, tim2)

_i2c = I2C(1, I2C.MASTER) # Setting up the I2C connection
_i2c.init() 
imu = BNO055.BNO(_i2c) # passes the I2C into the IMU driver

def calibrator():
    
    imu.Op_mode('NDOF')
    state = 0
    pos1 = encoder.get_position() # Should be 0,0
    print(pos1)
    print('Pos1')
    
    while imu.calib() != 'calibrated':
        #try:
            encoder.update()
            if state == 0:
                # negative Motor 1 = positive change in ticks (a change of 300 is good)
                pos2 = encoder.get_position() # ideal is 300,0
                print(pos2)
                print('Pos2')
                
                if pos2[0] <= 250:
                    M1.set_level(-50)
                    
                else:
                    M1.set_level(0)
                    state = 2
                    
            if state == 1:
                pos3 = encoder.get_position()
                print(pos3)
                print('Pos3')
                
                if pos3[1] <= 200: # goes down to 250
                    M2.set_level(-50)
                    
                else:
                    M2.set_level(20)
                    state = 2
                    
            if state == 2:
                pos4 = encoder.get_position()
                print(pos4)
                print('Pos4')
                
                if pos4[0] >= -100: # Lowest is -200
                    M1.set_level(50)
                    utime.sleep(0.01)
                else:
                    M1.set_level(0)
                    state = 0 
                    
            if state == 3:
                pos5 = encoder.get_position()
                print(pos5)
                print('Pos5')
                
                if pos5[1] >= -50: # lowest is -100
                    M2.set_level(50)
                    utime.sleep(0.01)
                else:
                    M2.set_level(-20)
                    state = 0
            
            
        # except KeyboardInterrupt:
        #     M1.set_level(0)
        #     M2.set_level(0)
    
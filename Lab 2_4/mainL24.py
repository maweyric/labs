"""
@file       mainL24.py
@brief      Initializes the I2C and records data at a specified time interval
@details    The program initializes the I2C for pins B6-9, and then sends it 
            to our temp sensor class. It also initializes and takes data using 
            the onboard temp sensor
@author     Mathis Weyrich            
@Date       Thu May  6 12:36:16 2021
@copyright  License Info


"""

# When using the double board - nucleo is COM5
# Board is COM 6


import pyb, utime, uos
from mcp9808 import tempsens
from pyb import I2C

_B8 = pyb.Pin(pyb.Pin.cpu.B8, mode = pyb.Pin.AF_OD, af = 4) # To ensure the pins are formatted correctly
_B9 = pyb.Pin(pyb.Pin.cpu.B9, mode = pyb.Pin.AF_OD, af = 4) 

##@brief Creates the I2C channel that correlates with pins 6,7 or 8,9 (from alternative function)
#

i2c = I2C(1, I2C.MASTER)
i2c.init(I2C.MASTER)


_adcall = pyb.ADCAll(12, 0x70000) # Setting up ADcall
_v = _adcall.read_core_vref() # Need to run this to get correct temp values

##@brief Choose how many minutes you want to take data
#

ttime = 60*8 # Total time (in minutes)

_sensor = tempsens(0x18, i2c) # Setting up the temp sensor

if __name__ == "__main__":
    try:
        with open ("NucleoTemp.csv", "w") as file:
            #csvwriter = csv.writer(file)
            for _x in range(ttime):
                _Ntemp = _adcall.read_core_temp()
                _Stemp = _sensor.celcius() 
                file.write("{:},{:},{:}\r\n".format(_x,_Ntemp, _Stemp))
                print(_x,_Ntemp,_Stemp)
                utime.sleep(60)
                    
            
        print("Finished")
    except KeyboardInterrupt:
        uos.sync()
"""

@file       mcp9808.py
@brief      Uses the I2C to communicate with the tempsensor
@details    The program initializes the I2C for pins B6-9, and then sends it 
            to our temp sensor class. It also initializes and takes data using 
            the onboard temp sensor
@author     Mathis Weyrich            
@Date       Thu May  6 12:42:42 2021
@copyright  License Info

"""

# use mem_read, mem_write from I2C

import pyb, ustruct

class tempsens:
    
    def __init__ (self, glob_ad, I2C):
        '''
        @brief          Uses the I2C to communicate with the tempsensor
        @details        Has 3 methods: check, celcius, and farenheight
        @param glob_ad  Is the address correlated with the sensor  
        @param I2C      The initialized I2C Master object

        '''
        
        self._glob_ad = glob_ad
        self._I2C = I2C
        self._data = bytearray([0,0])
        
        
    def check(self):
        '''
        @brief      Checks to make sure this is talking to the correct sensor

        '''
        self._address = self._I2C.mem_read(self._data, self._glob_ad, 0x6, timeout = 1500)
        self._cordata = ustruct.unpack('H',self._address)
        self._cordata = list(self._cordata)
        if self._cordata[0] == 21504:
            print('This is the correct temperature sensor')
            return True
        else:
            print('This is not the correct temperature sensor')
            return False
            
        
        
    def celcius(self):
        '''
        @brief      Takes the temperature data and returns it as a celcius value
        '''
        self._I2C.mem_read(self._data, self._glob_ad, 0x5, addr_size = 8)
        self._num = bin(int.from_bytes(self._data, "big"))[-12:]
        self._cdata = int(self._num,2)/15
        return self._cdata
    
    def farenheight(self):
        '''
        @brief      Converts the celcius values into farenheight values
        '''
        self._celcius()
        
        self._fdata = self._cdata*9/5 + 32
        return self._fdata
        # for farenheight - call self.celcius
        
        
        # for check function - check register ID = 0x0110
        # read address 
        # sensor address (global address) - use 18 = addr, 6 = mem arg
        # Register address = 6 (read from this register, and value should be 54 in hexadecimal)
        
        # use ustruct.unpack to combine 2 bytes
        # Use bytearray for a couple bytes
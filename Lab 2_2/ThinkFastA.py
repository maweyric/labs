"""@file ThinkFastA.py
@brief          A reaction time test using premade timer states to run the system
@details        Uses the utime function and the difference between ticks to:
                    1) Activate the light at a random time between 2-3 seconds
                    2) Leave the light on for 1 second
                    3) Calculate the reaction time by using the ticks when the light
                       went off, and you pressed the button.
                Each reaction time interval is initiated with a button press.
@author         Mathis Weyrich
@date           Thu Apr 22 12:58:22 2021
@copyright      License Info

"""

import pyb, utime, random
_state = 0
_x = 0
_TotReac = []
_button_push = False
print("started")

## @brief Setting the pin that activates when the desired button is pressed
#
Button = pyb.Pin (pyb.Pin.cpu.C13) # Setting a certain pin as a variable (button - B1)

## @brief Setting the pin that turns on the LED. 
#
LED = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
#tim2 = pyb.Timer(2, prescaler = 80, period = 0xfffffff) # Not quite sure, was used in the setup of a dimming LED
#LEDbright = tim2.channel(1, pyb.Timer.PWM, pin=LED) # Setting up the PWP


def onButtonPressFCN(IRQ_src):
    '''@brief Serves as the interrupt to switch between functions.
    @details When the button is pressed, it returns a global variable as true
             allowing the main code to go to the next function.
    '''
    global _button_push # Making the variable global is a cheap way of returning a value
    _button_push = True
    print("Button Pressed")
        
## @brief Setting up the button Interrupt
#
ButtonInt = pyb.ExtInt(Button, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)  
# Introducing the button inturrupt function        
 


if __name__ == "__main__":
    
    print("on")
    try:
        while True:

            if _state == 0:
                _state = 1
                
                print('Press the blue button to start the reaction time test')
                
            if _state == 1:
                
                if _button_push == True: # If the button is pressed, this causes the next _state to be brought forth.
                      
                    _button_push = False
                    _state = 2
                    _lightTime = random.randint(2000,3000)
                    _startTest = utime.ticks_ms()
                    #LEDbright.pulse_width_percent(100)
            if _state == 2:
                
                while utime.ticks_diff(utime.ticks_ms(), _startTest) < _lightTime:
                    pass
                
                LED.high()
                _state = 3
                _startLight = utime.ticks_us()
                    
            if _state == 3:
                
                if _button_push == True:
                    _button_push = False
                    _buttonTime = utime.ticks_us()
                    reactTime = utime.ticks_diff(_buttonTime,_startLight)# in nano Seconds
                    _TotReac.append(reactTime/1000000)
                    print("Your reaction time was", reactTime/1000, "in microseconds")
                    _x += 1
                    
                    
                if utime.ticks_diff(utime.ticks_us(),_startLight) >= 1000000:
                    #LEDbright.pulse_width_percent(0)
                    LED.low()
                    _state = 0
                    
                    _x -= 1
                    if _x < 0:
                        print("Sorry, too slow!")
                        state = 0
                        x = 0
                        _TotReac.append(1)
        
            
    except KeyboardInterrupt:
        if sum(_TotReac) != 0:
            _avgReac = sum(_TotReac)/len(_TotReac)
            print('Ctrl-c detected. Average reaction time was', _avgReac, " seconds")
        else:
            print("Try Pressing the button to get an average reaction time")
    
    
"""@file ThinkFastB.py
@brief          A reaction time test using the hardware timers to run the system
@details        Uses Output Compare and Input Compare methods to run the following system:
                    1) Activate the light at a random time between 2-3 seconds
                    2) Leave the light on for 1 second
                    3) Calculate the reaction time by using the ticks when the light
                       went off, and you pressed the button.
                Using the hardware timers allows the code to be much more concise than 
                when using the utime class.
@author         Mathis Weyrich
@date           Thu Apr 22 12:58:22 2021
@copyright      License Info

"""

import pyb, utime, random, micropython
_state = 0
_x = 0
_TotReac = []
_ICcap = None
_last_comp = 0
_new_comp = 0
_sstate = 1

## @brief   Setting the pin that activates when the desired button is pressed
#
Button = pyb.Pin (pyb.Pin.cpu.C13) # Setting a certain pin as a variable (button - B1)

## @brief   Setting the pin that turns on the LED. 
#
LED = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP) # Setting up a the LED Pin to toggle with High/Low

## @brief   Sets up the interrupt pin
# @details  The interrupt pin activated when the button is pressed, which eliminates
#           the need for a an external interupt function
#
pB3 = pyb.Pin(pyb.Pin.cpu.B3) # Setting up the interrupt pin for the IC

## @brief   Creates a timer
# @details  The timer has a period of 65535 and a prescaler of 7999. 
#           This means that the timer has a resolution of 1x10^-4 seconds, or 0.1 ms.
#
tim2 = pyb.Timer(2, prescaler = 7999, period= 0xffff) # has a resolution of 1x10^-4 s, results in 6.55 second period

## @brief   Setting up channel 2 of the timer as an IC
#
IC = tim2.channel(2, pyb.Timer.IC, pin=pB3, polarity=pyb.Timer.FALLING) # Timer Channel 2 for IC -





def ICCap(tim2):
    '''@brief Serves as the interrupt to capture the time when the button is pressed
    
    '''
    global _ICcap 
    _ICcap = IC.capture() # Stores the time in a global variable
           
      
 

def onOC(tim2):
    '''@brief   Serves as the mini FSM machine for the LED
    @details    When the an output compare match is activated, it toggles the LED.
                After the LED is on, a value equivalent to 1 second is added to
                the compare value. After the LED is toggled off, a random value
                between 2-3 seconds is added to the compare value.
    '''
    global _last_comp

    #_last_comp = tim2.counter() # Sets a variable as the counter value when the interrupt is activated
     
    if LED.value() == 1:
        print("On")
        _last_comp += 10000  # A set number of counts are added - equal to 1 seconds - to try and activate the OC at a later count.
    if LED.value() != 1:
        print("off")
        _last_comp += random.randint(20000, 30000)
    if _last_comp >=65535:
        _last_comp &=65535 # Dealing with overflow 
    OC.compare(_last_comp) # setting the new compare-match value

IC.callback(ICCap)  # IC is used to tell when the button has been pressed and capture that time

## @brief Setting up channel 1 of the timer as an OC to toggle the LED
#
OC = tim2.channel(1, mode=pyb.Timer.OC_TOGGLE, pin= LED, callback = onOC, polarity=pyb.Timer.LOW)
if __name__ == "__main__":
    
    print('Press the blue button when you see the light')   
    try:
        micropython.alloc_emergency_exception_buf(100) # Stores memory to capture information about potential errors in callback functions
        while True:

            if _state == 0:
                
                if _sstate == 1:
                    if LED.value(): # Activates when the value is high (LED ON)
                            #print(_last_comp,"Count for next toggle|", tim2.counter(), "Current Counter:Light On")
                            _LightUp = tim2.counter()
                            _sstate = 2
                if _sstate == 2:
                    if LED.value() != 1: # Activates when the value is low (LED OFF)
                           _sstate = 1
                        
                    
                if _ICcap != None:
                    _x = (_ICcap-_LightUp)&0xffff
                    _TotReac.append(_x/10000) # Adds the reaction times to a list
                    print(_TotReac, "Total Reaction", _ICcap, "ICcap", _LightUp, "Lightup")
                    _ICcap = None
                    
                    # if LED.value():
                    #     _new_comp = 10000 + _last_comp
                    # else:
                    #     _new_comp = int(random.randint(2000, 3000)/1000 * (80*10**6)/8000) + _last_comp
                   
            
    except KeyboardInterrupt:
        if sum(_TotReac) != 0:
            _avgReac = sum(_TotReac)/len(_TotReac) # Calculating the average reaction time 
            print('Ctrl-c detected. Average reaction time was', _avgReac, " seconds")
            tim2.deinit()
            
        else:
            print("Try Pressing the button to get an average reaction time")
            tim2.deinit()
# Example Numbers:
# Payment = (3,0,0,2,1,0,0,1) - pennies, nickles, dimes, quarters, $1, $5, $10, $20
# Price =     


def getChange(price, payment):
    import math
    Tpayment = payment['Twenty Dollars']*2000 + payment['Ten Dollars']*1000 + \
    payment['Five Dollars']*500 + payment['One Dollar']*100 + payment['Quarter']*25 + \
    payment['Dimes']*10 + payment['Nickles']*5 + payment['Pennies']
    change = {
    'Pennies': 0,
    'Nickles': 0,
    'Dimes': 0,
    'Quarter': 0,
    'One Dollar': 0,
    'Five Dollars': 0,
    'Ten Dollars': 0,
    'Twenty Dollars': 0}
    
    if price*100 <= Tpayment:
        TChange = Tpayment - price*100
        print(TChange)
            
        if TChange/2000 >= 1:
            change['Twenty Dollars']=math.floor(TChange/2000)
            TChange -= 2000*math.floor(TChange/2000)
            print('Twenty',TChange)
        if TChange/1000 >= 1: 
            change['Ten Dollars'] = math.floor(TChange/1000)
            TChange -= 1000*math.floor(TChange/1000)
            print('Ten',TChange)
        if TChange/500 >= 1: 
            change['Five Dollars']= math.floor(TChange/500)
            TChange -= 500*math.floor(TChange/500)
            print('Five',TChange)
        if TChange/100 >= 1: 
            change['One Dollar']=math.floor(TChange/100)
            TChange -= 100*math.floor(TChange/100)
            print('one',TChange)
        if TChange/25 >= 1: 
            change['Quarter']=math.floor(TChange/25)
            TChange -= 25*math.floor(TChange/25)
            print('quarter',TChange)
        if TChange/10 >= 1: 
            change['Dimes']= math.floor(TChange/10)
            TChange -= 10*math.floor(TChange/10)
            print('dimes',TChange)
        if TChange/5 >= 1: 
            change['Nickles']= math.floor(TChange/5)
            TChange -= 5*math.floor(TChange/5)
            print('Nickles',TChange)
        if TChange/1 >= 1: 
            change['Pennies']= math.floor(TChange/1)
            TChange -= 1*math.floor(TChange/1)
            print('Pennies',TChange)
        
        return change
        
    else:
        return False

if __name__ == "__main__":
    
    payment = {
        'Pennies': 4,
        'Nickles': 0,
        'Dimes': 2,
        'Quarter': 3,
        'One Dollar': 4,
        'Five Dollars': 1,
        'Ten Dollars': 1,
        'Twenty Dollars': 0}
    price = 4.99
    
    x = 'Not Done'

    while x != 'Done':
        change = getChange(price, payment)
        if change != False:
            x = 'Done'
            print('Total Change is:\r\n',change['Twenty Dollars'],'Twenty Dollar Bills,\r\n',change['Ten Dollars'],'Ten Dollar Bills,\r\n',change['Five Dollars'],'Five Dollar Bills,\r\n',change['One Dollar'],'One Dollar Bills,\r\n',change['Quarter'],'Quarters,\r\n',change['Dimes'],'Dimes,\r\n',change['Nickles'],'Nickles,\r\n',change['Pennies'],'Pennies,\r\n')
        elif change == False:
            x = 'Done'
            print('Need More Money!')
    
    
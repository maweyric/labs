# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 11:14:45 2021

@author: Mathis
"""

# Code Snippet

import time
import random


# Function Definitions go here

def motor_cmd(cmd):
    if cmd == '1':
        print('Elevator going up')
    elif cmd == '2':
        print('Elevator going down')
    elif cmd == '0':
        print('Elevator Stopped')
        
def B_sensor():
    return random.choice([True, False])

def go_button():
    return random.choice([1,2,3,4])

def e_button():
    return random.choice([1,2,3])



# Main program / test program begin
# This code only runs if the script is executed as main by pressing play
# but does not run if the script is imported as a module
if __name__ == "__main__":
    # Program initialization goes here
    state = 0 # Initial state is the init state
    workday = 0
    c = 0
    c_e = 0
    print('Welcome to the 3 story elevator')
    
while state <= 1: 
    if B_sensor(): #checks to see if elevator is at bottom floor
        print('Elevator at 1st Floor.')
        motor_cmd('0') 
        state = 2 # Updating state (on bottom floor)
        
    else:
        motor_cmd('2')
        state = 1 # The elevator is still going down
        
    time.sleep(2)
    
while state > 1: # Makes sure the elevator has reset to the bottom position
    
    
    if state ==2 : # At bottom floor 
        print('Waiting for customers')
        
        if c_e | go_button() == 1:
            c_e = 0
            print('Customer is at the first floor')
            if e_button() == 1: # Button selected is F1
                print('Customer delivered to floor 1')
                state = 2
                
            elif e_button() == 2: # Button selected is F2                
                print('Customer going to floor 2')
                motor_cmd('1')
                state = 3
                c = 1
            
                
            elif e_button() == 3: # Button selected is F2
                print('Customer going to floor 3')
                motor_cmd('1')
                state = 5
                c = 1
              
                
        elif go_button() == 2:
            print('Customer at second floor')
            motor_cmd('1')
            state = 3 # going up to floor 2
            c_e = 2
         
            
        elif go_button() == 3:
            print('Customer at third floor')
            motor_cmd('1')
            state = 5 # going up to floor 3
            c_e = 3
           
        
         
    elif state ==3:
        print('Arrived at 2nd floor.')
        motor_cmd('0')
        state = 4
        if c == 1:
            print('Customer delivered')
            c = 0
        
         
    elif state ==4:
                
        if c_e | go_button() == 2:
            c_e = 0
            print('Customer is at the second floor')
            if e_button() == 1: # Button selected is F1
                print('Customer going to floor 1')
                motor_cmd('2')
                state = 8
                c = 1
            
                
            elif e_button() == 2: # Button selected is F2
                print('Customer delivered to floor 2')
                state = 4
                                
            elif e_button() == 3: # Button selected is F2
                print('Customer going to floor 3')
                motor_cmd('1')
                state = 5
                c = 1
            
            
        elif go_button() == 1:
            print('Customer at first floor')
            motor_cmd('2')
            state = 8 # going down to floor 1
            c_e = 1
         
                            
        elif go_button() == 3:
            print('Customer at third floor')
            motor_cmd('1')
            state = 5 # going up to floor 3
            c_e = 3 
         
            
    elif state ==5:
        print('Arrived at 3rd floor.')
        motor_cmd('0')
        state = 6
        if c == 1:
            print('Customer delivered')
            c = 0
            
    elif state ==6:
                
        if c_e | go_button() == 3:
            c_e = 0
            print('Customer is at the third floor')
            if e_button() == 1: # Button selected is F1
                print('Customer going to floor 1')
                motor_cmd('2')
                state = 8
                c = 1
             
                
            elif e_button() == 2: # Button selected is F2
                print('Customer going to floor 2')
                motor_cmd('2')
                state = 7
                c = 1
           
                                
            elif e_button() == 3: # Button selected is F2
                print('Customer delivered to floor 3')
                state = 6
        
        elif go_button() == 1:
            print('Customer at first floor')
            motor_cmd('2')
            state = 8 # going down to floor 1
            c_e = 1

                            
        elif go_button() == 2:
            print('Customer at second floor')
            motor_cmd('2')
            state = 7 # going down to floor 2
            c_e = 2
           
            
    elif state ==7:
        print('Arrived at 2nd floor.')
        motor_cmd('0')
        state = 4
        if c == 1:
            print('Customer delivered')
            c = 0

    elif state ==8:
        print('Arrived at 1st floor.')
        motor_cmd('0')
        state = 2
        if c == 1:
            print('Customer delivered')
            c = 0
    
    if workday > 8:
        state = 0
        print('The workday is over, elevator no longer active')
        
    time.sleep(2)
    
    workday = workday + 0.4
    

            

            
    # Program de-initialization goes here
    
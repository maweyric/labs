'''@brief   Simulating a 2-story elevator.
@details    Using a finte state machine to model the behavior of a simple
            elevator.
@author     Mathis Weyrich
@date       Tue Jan 19 11:14:45 2021
@copyright  License Info
'''

# Code Snippet

import time      # Importing a timer to be used to "sleep" execution
import random    # Importing a random generator for our "customer" inputs 


# Function Definitions go here

def motor_cmd(cmd):
    ''' @brief Commands motor to do go up, down, or stop
        @param cmd The command to give the motor to move
    '''
    if cmd == '1':
        print('Elevator going up')
    elif cmd == '2':
        print('Elevator going down')
    elif cmd == '0':
        print('Elevator Stopped')
        
def B_sensor():    # Sensing whether the elevator reached the bottom
    return random.choice([True, False])

def go_button_1(): # Providing a random "customer" input at first floor
    return random.choice([True, False])

def go_button_2(): # Providing a random "customer" input at first floor
    return random.choice([True, False])



# Main program / test program begin
# This code only runs if the script is executed as main by pressing play
# but does not run if the script is imported as a module
if __name__ == "__main__":
    # Program initialization goes here
    state = 0 # Initial state is the init state
    workday = 0
    print('Initializing')
    
    
# Following while loop only occurs when initializing. 
# Makes sure that the elevator has descended and starts on the bottom floor.
while state <= 1: 
    if B_sensor():
        print('Elevator at 1st Floor.')
        motor_cmd('0') 
        state = 2 # Updating state to arriving at floor 1
        
    else:
        motor_cmd('2')
        state = 1 # Updating state to say that the elevator has still not
                  # reached the bottom floor
        
    time.sleep(1)
    
while state > 1:
    try:
    
        if state ==2 : # If the elevator is at floor 1
            print('Waiting for customers going up')
            if go_button_1(): # Waiting for a "customer" input to go up
                motor_cmd('1')
                state = 3    
             
        elif state ==3: # Elevator moving up to second/arriving
            print('Arrived at 2nd floor')
            motor_cmd('0')
            state = 4    
             
        elif state ==4: # If elevator is at floor 2
            print('Waiting for customers going down')
            if go_button_2():# Waiting for a "customer" input to go down
                motor_cmd('2')
                state = 5
             
        elif state ==5: # Elevator moving down to first/arriving
            print('Elevator at 1st Floor')
            motor_cmd('0')
            state = 2
        
        if workday > 8: # Loop to end activation of the elevator
            state = 0
            print('The workday is over, elevator no longer active')
        
        workday = workday + 0.2
        
        time.sleep(1) # to Throttle Execution
        
    except KeyboardInterrupt:
                # This except block catches "Control + C" from the keyboard to end
                # while(True) loop when desired
            break

    # Program de-initialization goes here
    
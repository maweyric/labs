"""
@file       CSide.py
@brief      The computer side of a direct communication link 
@details    The computer initializes a serial port with the nucleo for direct
            communication. The computer sends a specific string based on a 
            keyboard input. Once the read code is sent, the computer waits for 
            the passcode, at which points it creates an appropriately sized 
            array and ports the data from the nucleo into a CSV file.
            
@author     Mathis Weyrich            
@Date       Thu Apr 29 12:02:47 2021
@copyright  License Info
"""
from matplotlib import pyplot
import serial,keyboard, csv
from array import array
ser = serial.Serial(port='COM4',baudrate=115273,timeout=1) #If using the large board with 2 ports, send this to the nucleo and connect using putty to the other port
_usr = 'yes'
_state = -1

## @brief   A string sent by the nucleo once data collection is finished
# @details  The string sent has 2 values - a key that signifies that the
#           code has been collected denoted as a "1". The other value 
#           carries the length of the data array being sent over so the
#           computer side can efficiently set up an array
#
passcode = 0
_ready = False
 
_last_key= None
_inv = 0

def kb_cb(key):
    """ Callback function which is called when a key has been pressed.
    """
    global _last_key
    _last_key = key.name
    
#keyboard.on_release_key("S", callback=kb_cb)
keyboard.on_release_key("g", callback=kb_cb)

while _usr == 'yes':
    if _state == -1:
        print('To acquire a data recording type in "G".')
        _state = 0
    
    if _state == 0:
        
        try:
            if _last_key is not None:
                _inv = _last_key
                _last_key = None
        except KeyboardInterrupt:
            break
        if _inv == 'G' or _inv =='g':
            #_inv = input('To acquire a data recording type in "G".')
            ser.write(str(_inv).encode('ascii')) #103 or 71
            _state = 1
            
        
    if _state ==1:
        print('Waiting for pass')
        passcode = ser.readline().decode()
        passcode = passcode.strip()
        if len(passcode) != 0:
            passcode = passcode.split(',')
            _state = 3
            
            
    if _state == 3:
        if int(passcode[0]) == 1:
            
            ## @brief The counts collected by the nucleo will end up in the Values array
            #
            
            values = array('f',(int(passcode[1]))*[0])
            
            ## @brief The times corresponding with the values collected by the nucleo are stored in the Times array.
            #
            
            times = array('f',(int(passcode[1]))*[0])
            with open('Data.csv','w') as file:
                writer = csv.writer(file, dialect='excel', lineterminator='\n')
                writer.writerow(['Times']+['Position']+['Speed'])
            
                for x in range (int(passcode[1])):
                    _raw_data = ser.readline().decode()
                    _rough_data = _raw_data.strip()
                    if len(_rough_data) != 0:
                        _polished_data = _rough_data.split(',')
                        values[x] = float(_polished_data[1])
                        times[x] = float(_polished_data[0])
                        
                        writer.writerow([times[x]]+[values[x]])
                
                    _ready = True
        
            
        
        if _ready == True:
            fig, axs = pyplot.subplots(1, sharex = True)
            fig.suptitle('Positional and Speed Data of the Motor')
            axs.plot(times[:-1], values[:-1])
            #pyplot.xlabel('Time')
            axs.set_ylabel('Data')
            
            axs.set_xlabel('Time')
            
            _ready = False
            _state = -1

ser.close()
keyboard.unhook_all()
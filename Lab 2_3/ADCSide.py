"""
@file       ADCSide.py
@brief      The nucleo side of a direct communication link sending data from an ADC
@details    The nucleo recieves commands from the computer that prompts it to 
            start taking data. This data is then sent to the computer for further processing.
            The data collected is the ADC counts of an onboard button.
@author     Mathis Weyrich            
@Date       Thu Apr 29 12:34:22 2021
@copyright  License Info
"""
import utime, math as m
import array, micropython
from pyb import UART
import pyb

pyb.repl_uart(None) # Prevents things like print("stuff") from being sent.

class ADCcoms:
    
    def __init__(self):
        '''
        @brief  Initializing variables and the communication port
        '''

        self.myuart = UART(2) # Reference to the physical port that will work
        
        self._state = 0

        self._val = 0
        
        ## @brief   Setting the pin that activates when the desired button is pressed
        #
        self.Button = pyb.Pin (pyb.Pin.cpu.C13)
        
        ## @brief   Sets up a timer at a frequency to collect ~100 data points during the test
        #
        
        self.tim2 = pyb.Timer(2, freq = 50000) #100000 for 200 data points, 75000 for 150 data points, 50000 for 100 data points
        
        ## @brief   The pin connected to the ADC
        #
        
        self.pA0 = pyb.Pin(pyb.Pin.cpu.A0)
        
        self.adc = pyb.ADC(self.pA0) # Setting up the ADC
            
    def run(self):
        '''
        @brief   Runs a finite state machine that obtains and sends data from the ADC
        @details The initial state waits for an input signal from the computer
                 side and sets the following state based on the input. A buffer 
                 array is initialized to allow the ADC to fill it at the set frequency.
                 The data is only deemed usable if a button pressed is found. 
                 Following the end of data collection the nucleo sends an initial 
                 passcode to the computer with the amount of data to be sent. 
                 Following that, the data is sent in a csv style string.

        '''
        
        if self._state == 0:
            
            if self.myuart.any() != 0:
                self._val = self.myuart.readchar()
                
            if self._val == 71 or self._val == 103: # Reading 'g'
                self._state = 1
                
                self._val = 0
            # if self._val == 84 or self._val == 116: #Reading 's'
            #     self._state = 3
            #     self._val = 0
                
        if self._state == 1:
            print("State 1")
        
            self._buffy = array.array('H', (0 for index in range (9750))) # Sets up a buffer array for the 
            # ADC.
            
            self.adc.read_timed(self._buffy,self.tim2)
            
            if max(self._buffy) - min(self._buffy) > 4075: #Ensures a button press has been collected 
                
                        self._state = 3
                        print("good data found")
            
    
        if self._state == 2:
            
            
            for x in range (len(self._buffy)):
                self.myuart.write('{:},{:}\r\n'.format(20*x,self._buffy[x])) # Time is in us
                
                print('sending data' + str(x))
                
            self._state = 0
            self._val = 0
            print('Done')
    
        if self._state == 3:
            print('_state 3')
            self.myuart.write('{:},{:}\r\n'.format(1,9750))
            self._state = 2
        
        
_nucleocomms = ADCcoms()
_x = True
while _x == True:
    try:
        micropython.alloc_emergency_exception_buf(100)
        _nucleocomms.run()
    except KeyboardInterrupt:
        _x = False
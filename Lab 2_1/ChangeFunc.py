"""@file ChangeFunc.py 
@brief      Function that returns the corresponding change based on an input price
@author     Mathis Weyrich
@date       Sat Apr 3 15:20:06 2021
@copyright  License Info    
"""   


def getChange(price, payment):
    '''
    @brief          Returns the corresponding change based on an input price.
    @details        Uses the input payment and price to identify the change in
                    the largest denominations possible. 
    @param price:   The input price of the item as a float
    @param payment: The input payment as a dictionary
    @return         If the payment is greater than the price, then a dictionary
                    with the corresponding change is returned. If the price is
                    larger than the payment, returns the string 'More'.

    '''
    import math
    _Tpayment = payment['Twenty Dollars']*2000 + payment['Ten Dollars']*1000 + \
    payment['Five Dollars']*500 + payment['One Dollar']*100 + payment['Quarters']*25 + \
    payment['Dimes']*10 + payment['Nickles']*5 + payment['Pennies']
    change = {
    'Pennies': 0,
    'Nickles': 0,
    'Dimes': 0,
    'Quarters': 0,
    'One Dollar': 0,
    'Five Dollars': 0,
    'Ten Dollars': 0,
    'Twenty Dollars': 0}
    
    if price*100 <= _Tpayment:
        _TChange = _Tpayment - price*100
        #print(_TChange)
            
        if _TChange/2000 >= 1:
            change['Twenty Dollars']=math.floor(_TChange/2000)
            _TChange -= 2000*math.floor(_TChange/2000)
            #print('Twenty',_TChange)
        if _TChange/1000 >= 1: 
            change['Ten Dollars'] = math.floor(_TChange/1000)
            _TChange -= 1000*math.floor(_TChange/1000)
            #print('Ten',_TChange)
        if _TChange/500 >= 1: 
            change['Five Dollars']= math.floor(_TChange/500)
            _TChange -= 500*math.floor(_TChange/500)
            #print('Five',_TChange)
        if _TChange/100 >= 1: 
            change['One Dollar']=math.floor(_TChange/100)
            _TChange -= 100*math.floor(_TChange/100)
            #print('one',_TChange)
        if _TChange/25 >= 1: 
            change['Quarters']=math.floor(_TChange/25)
            _TChange -= 25*math.floor(_TChange/25)
            #print('quarters',_TChange)
        if _TChange/10 >= 1: 
            change['Dimes']= math.floor(_TChange/10)
            _TChange -= 10*math.floor(_TChange/10)
            #print('dimes',_TChange)
        if _TChange/5 >= 1: 
            change['Nickles']= math.floor(_TChange/5)
            _TChange -= 5*math.floor(_TChange/5)
            #print('Nickles',_TChange)
        if _TChange/1 >= 1: 
            change['Pennies']= math.floor(_TChange/1)
            _TChange -= 1*math.floor(_TChange/1)
            #print('Pennies',_TChange)
        
        return change
        
    else:
        return 'More'

if __name__ == "__main__":
    
    payment = {
        'Pennies': 4,
        'Nickles': 0,
        'Dimes': 2,
        'Quarters': 3,
        'One Dollar': 4,
        'Five Dollars': 1,
        'Ten Dollars': 1,
        'Twenty Dollars': 0}
    _price = 4.99
    
    
    _x = 'Not Done'

    while _x != 'Done':
        _change = getChange(_price, payment)
        if _change != 'More':
            _x = 'Done'
            print('Total Change is:\r\n',_change['Twenty Dollars'],'Twenty Dollar Bills,\r\n',_change['Ten Dollars'],'Ten Dollar Bills,\r\n',_change['Five Dollars'],'Five Dollar Bills,\r\n',_change['One Dollar'],'One Dollar Bills,\r\n',_change['Quarters'],'Quarters,\r\n',_change['Dimes'],'Dimes,\r\n',_change['Nickles'],'Nickles,\r\n',_change['Pennies'],'Pennies,\r\n')
        elif _change == 'More':
            _x = 'Done'
            print('Need More Money!')
    
    
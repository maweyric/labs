## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_port Portfolio Details  
#  This is my ME405 Portfolio. See individual modules for detals.
#
#  Included modules are:
#  * Lab0x00 (\ref sec_lab0)
#  * Lab0x01 (\ref sec_lab1)
#  * Lab0x0N (\ref sec_labn)
#
#  @section sec_lab0 Lab0x00 Documentation
#  * Source: https://bitbucket.org/erwade/examplefiles/src/master/Lab0.py
#  * Documentation: \ref Lab0
#
#  @section sec_lab1 Lab0x01 Documentation 
#  * Source: https://bitbucket.org/erwade/examplefiles/src/master/Lab1.py
#  * Documentation: \ref Lab1
#
#  @section sec_labn Lab0x0N Documentation
#  * Source: https://bitbucket.org/erwade/examplefiles/src/master/LabN.py
#  * Documentation: \ref LabN
#
#  @author Eric Espinoza-Wade
#
#  @date Sept. 20, 2020
#
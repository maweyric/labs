"""@file myFile.py 
@brief      Main code to operate the Vendotron
@details    Operates a Vendotron selling 4 items and accepting payment in
            denominations from pennies to twenty dollars. A user may choose
            any option at any point and once sufficient payment is recieved
            any remaining change can be released or another drink can be 
            selected. 
            Inputs are using the keyboard. Possible inputs include:
                c,s,d,p,e,q,0,1,2,3,4,5,6,7
            A scrolling message runs accross the console after 90 seconds of inactivity.
@author     Mathis Weyrich
@date       Thur Apr 8 15:20:06 2021
@copyright  License Info    
"""

from time import sleep
import keyboard, time
from ChangeFunc import getChange
_last_Key = ''

## @brief   This is the variable that stores the payment of the user
# @details  The object 'payment' is in the form of a dictionary with the 
#           following values:
#           Pennies, Nickles, Dimes, Quarters, One Dollar, Five Dollars, 
#           Ten Dollars, Twenty Dollars.
#   
payment = {
        'Pennies': 0,
        'Nickles': 0,
        'Dimes': 0,
        'Quarters': 0,
        'One Dollar': 0,
        'Five Dollars': 0,
        'Ten Dollars': 0,
        'Twenty Dollars': 0}

def kb_cb(key):
    """ Callback function which is called when a key has been pressed.
    """
    global _last_Key
    _last_Key = key.name

# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("c", _callback=kb_cb) # Cuke
keyboard.on_release_key("s", _callback=kb_cb) # Spryte
keyboard.on_release_key("d", _callback=kb_cb) # Dr Pupper
keyboard.on_release_key("p", _callback=kb_cb) # popsi
keyboard.on_release_key("e", _callback=kb_cb) # Eject
keyboard.on_release_key("q", _callback=kb_cb) # Quit
keyboard.on_release_key("0", _callback=kb_cb) # Pennies
keyboard.on_release_key("1", _callback=kb_cb) # Nickles
keyboard.on_release_key("2", _callback=kb_cb) # Dimes
keyboard.on_release_key("3", _callback=kb_cb) # Quarter
keyboard.on_release_key("4", _callback=kb_cb) # 1 Dollar
keyboard.on_release_key("5", _callback=kb_cb) # 5 Dollars
keyboard.on_release_key("6", _callback=kb_cb) # 10 Dollars
keyboard.on_release_key("7", _callback=kb_cb) # 20 Dollars

def VendotronTask():
    """ Runs one iteration of the task
    """
    _state = 0
    global _last_Key
    global payment
    ## @brief The price of the selected object
    #
    O_price = 0
    _price = 0
    ## @brief The total payment by the user times 100
    #
    Tpayment = 0
    
    while True:
        # Implement FSM using a while loop and an if _statement
        # will run eternally until user presses CTRL-C
        
        
        if _state == 0:
            print('Welcome to the Vendotron\r\nPlease insert coins or choose from our many options:\r\nCuke: $1.00\r\nPopsi: $1.20\r\nSpryte: $0.85\r\nDr. Pupper: $1.10\r\n')
            # perform _state 0 operations
            # this init _state, initializes the FSM itself, with all the
            # code features already set up
            _start_time = time.localtime()
            #print("The _state is",_state)
            _state = 1       # on the next iteration, the FSM will run _state 1
            
        elif _state == 1:
            _run_time = time.localtime()
            if _last_Key == 'c':
                O_price = 100
                if O_price/100 != _price:
                    print('You have chosen a Cuke.')
                    _state = 3
                else:
                    pass
                _last_Key = ''
                
            elif _last_Key == 's':
                O_price = 85
                if O_price/100 != _price:
                    print('You have chosen a Spryte.')
                    _state = 3
                else:
                    pass
                _last_Key = ''
                
            elif _last_Key == 'd':
                O_price = 110
                if O_price/100 != _price:
                    print('You have chosen a Dr. Pupper.')
                    _state = 3
                else:
                    pass
                _last_Key = ''
                
            elif _last_Key=='p':
                O_price = 120
                if O_price/100 != _price:
                    print('You have chosen a Popsi.')
                    _state = 3
                else:
                    pass
                _last_Key = ''
            
            elif _last_Key=='0' or _last_Key=='1' or _last_Key=='2' or _last_Key=='3' or _last_Key=='4' or _last_Key=='5' or _last_Key=='6' or _last_Key=='7':
                _state = 2
            elif _last_Key == 'e':
                _state = 5
            elif _last_Key == 'q':
                print("'q' detected. Quitting...")
                keyboard.unhook_all ()
                break
            if (_run_time[4]+_run_time[5]/60) - (_start_time[4]+_start_time[5]/60) > 1.5:
                _state = 0
                
                
                
            # perform _state 1 operations
            
            
        elif _state == 2:
            if _last_Key=='0':
                payment['Pennies'] += 1
                _last_Key = ''
            elif _last_Key=='1':
                payment['Nickles'] += 1
                _last_Key = ''
            elif _last_Key=='2':
                payment['Dimes'] += 1
                _last_Key = ''
            elif _last_Key=='3':
                payment['Quarters'] += 1
                _last_Key = ''
            elif _last_Key=='4':
                payment['One Dollar'] += 1
                _last_Key = ''
            elif _last_Key=='5':
                payment['Five Dollars'] += 1
                _last_Key = ''
            elif _last_Key=='6':
                payment['Ten Dollars'] += 1
                _last_Key = ''
            elif _last_Key=='7':
                payment['Twenty Dollars'] += 1
                _last_Key = ''
            
            Tpayment = payment['Twenty Dollars']*2000 + payment['Ten Dollars']*1000 + \
            payment['Five Dollars']*500 + payment['One Dollar']*100 + payment['Quarters']*25 + \
            payment['Dimes']*10 + payment['Nickles']*5 + payment['Pennies']
            
            time.sleep(0.05)
            print('Total balance: $',Tpayment/100)
            
            if O_price == 0:
                print('Select a Drink')
                _state = 1
            elif O_price != 0:
                _state = 4
                
    
            
        elif _state == 3:
            _price = O_price/100
            print('That will be $', O_price/100)
            if Tpayment != 0:
                if Tpayment >= _price*100:
                    print('Total Balance: $', (Tpayment - _price*100)/100)
                    _state = 4
                else:
                    _state = 4
            elif Tpayment == 0:
                _state = 1   # s3 -> s1
            
        elif _state == 4:
            change = getChange(_price, payment)
            if change != 'More':
                time.sleep(0.1)
                print('\r\nThank you for shopping with the Vendetron')
                time.sleep(0.1)
                print('Please select another item to purchase or press "E" to recieve change\r\n')
                Tpayment -= O_price
                O_price = 0
                _price = 0
                payment = change
                _state = 5

            elif change == 'More':
                print('Need More Money!\r\n')
                _state = 1
                
        elif _state == 5:
            if _last_Key == 'e':
                print('Total Change is:\r\n',payment['Twenty Dollars'],'Twenty Dollar Bills,\r\n',payment['Ten Dollars'],'Ten Dollar Bills,\r\n',payment['Five Dollars'],'Five Dollar Bills,\r\n',payment['One Dollar'],'One Dollar Bills,\r\n',payment['Quarters'],'Quarters,\r\n',payment['Dimes'],'Dimes,\r\n',payment['Nickles'],'Nickles,\r\n',payment['Pennies'],'Pennies\r\n')
                payment = {
                    'Pennies': 0,
                    'Nickles': 0,
                    'Dimes': 0,
                    'Quarters': 0,
                    'One Dollar': 0,
                    'Five Dollars': 0,
                    'Ten Dollars': 0,
                    'Twenty Dollars': 0}
                _last_Key = ''
                _state = 0
                time.sleep(0.1)
            elif _last_Key != '':
                _state = 1
            
                
            
        else:
            # this _state shouldn't exist!
            
            pass
        
        yield(_state)



if __name__ == "__main__":
    # Initialize code to run FSM (not the init _state). This means building the 
    # iterator as generator object
    

    vendo = VendotronTask()
    
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
    
    try:
        while True:
            next(vendo)
            sleep(0.01)
            
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')
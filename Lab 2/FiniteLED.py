'''@file FiniteLED.py
@brief      Alternating LED behavior at the press of a button.
@details    Pressing the blue button on the Nucleo board will switch the LED
            between three different modes: Square, Sine, and Saw waves. 
            Use the following link for the source code:
            https://bitbucket.org/maweyric/labs/src/master/Lab%202/FiniteLED.py
@author     Mathis Weyrich with help from Matthew Nagy
@date       Thu Jan 28 08:42:31 2021
@copyright  License Info

'''

import pyb, utime, math # Importing in the nucleo board to be able to use  
                        # specific math, internal timer, and call out pins
state = 0 
start = 0
button_push = False # Introducing all the variables
Button = pyb.Pin (pyb.Pin.cpu.C13) # Setting a certain pin as a variable (button - B1)
LED = pyb.Pin (pyb.Pin.cpu.A5) # Setting a certain pin as a variable (LD2)

print('Press the blue button to cycle through different LED modes')

def onButtonPressFCN(IRQ_src):
    '''@brief Serves as the interrupt to switch between functions.
    @details When the button is pressed, it returns a global variable as true
             allowing the main code to go to the next function.
    '''
    global button_push # Making the variable global is a cheap way of returning a value
    button_push = True
        
ButtonInt = pyb.ExtInt(Button, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)  
# Introducing the button inturrupt function        
 
while True:
    tim2 = pyb.Timer(2, freq = 20000) # Not quite sure, was used in the setup of a dimming LED
    LEDbright = tim2.channel(1, pyb.Timer.PWM, pin=LED) # Setting up the PWP
    
    if state ==0: # Providing an initialization phase
        if button_push == True: # If the button is pressed, this causes the next state to be brought forth.
            print("'Blinking' pattern initiated")
            button_push = False
            state = 1
            start = utime.ticks_ms()

# Using if statements rather than while statements allows for almost instantaneous 
# switching of the state. 
    if state == 1:
        if utime.ticks_diff(utime.ticks_ms(), start) <= 500: # Comparing the
        # start time to the current internal timer to make sure the LED is on for .5 seconds
        # The diff function can also deal with "wrap around" when the internal time resets
            LEDbright.pulse_width_percent(100) # LED at full brightness
        elif utime.ticks_diff(utime.ticks_ms(), start) >= 1000:
            start = utime.ticks_ms() # Restarts the process once the timer gets larger.
        else:
            LEDbright.pulse_width_percent(0) # LED off
        if button_push == True:
            print("'Sine Wave' pattern initiated")
            button_push = False
            state = 2
            start = utime.ticks_ms()
            
    if state == 2:
        # creating a variable dependant on the time. Divided by 10,000 (division + 
        # modulus essentially the same) to turn milliseconds into a 10 second
        # Block. 
        rad = (utime.ticks_diff(utime.ticks_ms(), start)/100 % 100)
        # The actual mathmatical function that will represent the sin wave
        x = 50*math.sin(rad/50*math.pi)+50
        LEDbright.pulse_width_percent(x)
        if button_push == True:
            print("'Saw Wave' pattern initiated")
            button_push = False
            state = 3
            start = utime.ticks_ms()
    
    if state == 3:
        # Similar as for the sign wave - this variable is overall dividing the
        # time increment by 1000, turning the function into a 1 second function.
        saw = utime.ticks_diff(utime.ticks_ms(), start)/10
        duty = (saw % 100)
        LEDbright.pulse_width_percent(duty)
        if button_push == True:
            print("'Blinking' pattern initiated")
            button_push = False
            state = 1
            start = utime.ticks_ms()

        